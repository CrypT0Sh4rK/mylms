<?php

/* default/learnpath/list.tpl */
class __TwigTemplate_17627c73bd80594a29d88a699eb09cd3a97976f8c1d1b0c97e5c8b2ff33d593f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script>
    function confirmation(name) {
        if (confirm(\"";
        // line 3
        echo get_lang("AreYouSureToDeleteJS");
        echo " \\\"\" + name + \"\\\" ?\")) {
            return true;
        } else {
            return false;
        }
    }
</script>
";
        // line 10
        $context["configuration"] = api_get_configuration_value("lp_category_accordion");
        // line 11
        echo "<div class=\"lp-accordion panel-group\" id=\"lp-accordion\" role=\"tablist\" aria-multiselectable=\"true\">
    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["data"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["lp_data"]) {
            // line 13
            echo "        ";
            $context["show_category"] = true;
            // line 14
            echo "
        ";
            // line 15
            if ((($context["filtered_category"] ?? null) && (($context["filtered_category"] ?? null) != $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "id", array())))) {
                // line 16
                echo "            ";
                $context["show_category"] = false;
                // line 17
                echo "        ";
            }
            // line 18
            echo "
        ";
            // line 19
            if (($context["show_category"] ?? null)) {
                // line 20
                echo "            ";
                if ((($context["configuration"] ?? null) == 0)) {
                    // line 21
                    echo "                <!--- old view -->
                ";
                    // line 22
                    if (((twig_length_filter($this->env, ($context["categories"] ?? null)) > 1) && $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "id", array()))) {
                        // line 23
                        echo "                    ";
                        if (($context["is_allowed_to_edit"] ?? null)) {
                            // line 24
                            echo "                        <h3 class=\"page-header\">
                            ";
                            // line 25
                            echo $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getName", array(), "method");
                            echo "

                            ";
                            // line 27
                            if (($this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method") > 0)) {
                                // line 28
                                echo "                                ";
                                if ( !$this->getAttribute(($context["_c"] ?? null), "session_id", array())) {
                                    // line 29
                                    echo "                                    <a href=\"";
                                    echo ((("lp_controller.php?" . $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array())) . "&action=add_lp_category&id=") . $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method"));
                                    echo "\"
                                       title=\"";
                                    // line 30
                                    echo get_lang("Edit");
                                    echo "\">
                                        <img src=\"";
                                    // line 31
                                    echo Template::get_icon_path("edit.png");
                                    echo "\" alt=\"";
                                    echo get_lang("Edit");
                                    echo "\">
                                    </a>

                                    ";
                                    // line 34
                                    if ($this->getAttribute(($context["subscription_settings"] ?? null), "allow_add_users_to_lp_category", array())) {
                                        // line 35
                                        echo "                                        <a href=\"";
                                        echo ((("lp_controller.php?" . $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array())) . "&action=add_users_to_category&id=") . $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method"));
                                        echo "\"
                                           title=\"";
                                        // line 36
                                        echo get_lang("AddUsers");
                                        echo "\">
                                            <img src=\"";
                                        // line 37
                                        echo Template::get_icon_path("user.png");
                                        echo "\" alt=\"";
                                        echo get_lang("AddUsers");
                                        echo "\">
                                        </a>
                                    ";
                                    }
                                    // line 40
                                    echo "
                                    ";
                                    // line 41
                                    if (($this->getAttribute($context["loop"], "index0", array()) == 1)) {
                                        // line 42
                                        echo "                                        <a href=\"#\">
                                            <img src=\"";
                                        // line 43
                                        echo Template::get_icon_path("up_na.png");
                                        echo "\" alt=\"";
                                        echo get_lang("Move");
                                        echo "\">
                                        </a>
                                    ";
                                    } else {
                                        // line 46
                                        echo "                                        <a href=\"";
                                        echo ((("lp_controller.php?" . $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array())) . "&action=move_up_category&id=") . $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method"));
                                        echo "\"
                                           title=\"";
                                        // line 47
                                        echo get_lang("Move");
                                        echo "\">
                                            <img src=\"";
                                        // line 48
                                        echo Template::get_icon_path("up.png");
                                        echo "\" alt=\"";
                                        echo get_lang("Move");
                                        echo "\">
                                        </a>
                                    ";
                                    }
                                    // line 51
                                    echo "
                                    ";
                                    // line 52
                                    if (((twig_length_filter($this->env, ($context["data"] ?? null)) - 1) == $this->getAttribute($context["loop"], "index0", array()))) {
                                        // line 53
                                        echo "                                        <a href=\"#\">
                                            <img src=\"";
                                        // line 54
                                        echo Template::get_icon_path("down_na.png");
                                        echo "\" alt=\"";
                                        echo get_lang("Move");
                                        echo "\">
                                        </a>
                                    ";
                                    } else {
                                        // line 57
                                        echo "                                        <a href=\"";
                                        echo ((("lp_controller.php?" . $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array())) . "&action=move_down_category&id=") . $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method"));
                                        echo "\"
                                           title=\"";
                                        // line 58
                                        echo get_lang("Move");
                                        echo "\">
                                            <img src=\"";
                                        // line 59
                                        echo Template::get_icon_path("down.png");
                                        echo "\" alt=\"";
                                        echo get_lang("Move");
                                        echo "\">
                                        </a>
                                    ";
                                    }
                                    // line 62
                                    echo "                                ";
                                }
                                // line 63
                                echo "
                                ";
                                // line 64
                                if (($this->getAttribute($context["lp_data"], "category_visibility", array()) == 0)) {
                                    // line 65
                                    echo "                                    <a href=\"lp_controller.php?";
                                    echo (($this->getAttribute(($context["_p"] ?? null), "web_cid_query", array()) . "&") . twig_urlencode_filter(array("action" => "toggle_category_visibility", "id" => $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "id", array()), "new_status" => 1)));
                                    echo "\"
                                       title=\"";
                                    // line 66
                                    echo get_lang("Show");
                                    echo "\">
                                        <img src=\"";
                                    // line 67
                                    echo Template::get_icon_path("invisible.png");
                                    echo "\" alt=\"";
                                    echo get_lang("Show");
                                    echo "\">
                                    </a>
                                ";
                                } else {
                                    // line 70
                                    echo "                                    <a href=\"lp_controller.php?";
                                    echo (($this->getAttribute(($context["_p"] ?? null), "web_cid_query", array()) . "&") . twig_urlencode_filter(array("action" => "toggle_category_visibility", "id" => $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "id", array()), "new_status" => 0)));
                                    echo "\"
                                       title=\"";
                                    // line 71
                                    echo get_lang("Hide");
                                    echo "\">
                                        <img src=\"";
                                    // line 72
                                    echo Template::get_icon_path("visible.png");
                                    echo "\" alt=\"";
                                    echo get_lang("Hide");
                                    echo "\">
                                    </a>
                                ";
                                }
                                // line 75
                                echo "
                                ";
                                // line 76
                                if (($this->getAttribute($context["lp_data"], "category_is_published", array()) == 0)) {
                                    // line 77
                                    echo "                                    <a href=\"lp_controller.php?";
                                    echo (($this->getAttribute(($context["_p"] ?? null), "web_cid_query", array()) . "&") . twig_urlencode_filter(array("action" => "toggle_category_publish", "id" => $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "id", array()), "new_status" => 1)));
                                    echo "\"
                                       title=\"";
                                    // line 78
                                    echo get_lang("LearnpathPublish");
                                    echo "\">
                                        <img src=\"";
                                    // line 79
                                    echo Template::get_icon_path("lp_publish_na.png");
                                    echo "\"
                                             alt=\"";
                                    // line 80
                                    echo get_lang("LearnpathPublish");
                                    echo "\">
                                    </a>
                                ";
                                } else {
                                    // line 83
                                    echo "                                    <a href=\"lp_controller.php?";
                                    echo (($this->getAttribute(($context["_p"] ?? null), "web_cid_query", array()) . "&") . twig_urlencode_filter(array("action" => "toggle_category_publish", "id" => $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "id", array()), "new_status" => 0)));
                                    echo "\"
                                       title=\"";
                                    // line 84
                                    echo get_lang("LearnpathDoNotPublish");
                                    echo "\">
                                        <img src=\"";
                                    // line 85
                                    echo Template::get_icon_path("lp_publish.png");
                                    echo "\" alt=\"";
                                    echo get_lang("Hide");
                                    echo "\">
                                    </a>
                                ";
                                }
                                // line 88
                                echo "                                ";
                                if ( !$this->getAttribute(($context["_c"] ?? null), "session_id", array())) {
                                    // line 89
                                    echo "                                    <a href=\"";
                                    echo ((("lp_controller.php?" . $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array())) . "&action=delete_lp_category&id=") . $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method"));
                                    echo "\"
                                       title=\"";
                                    // line 90
                                    echo get_lang("Delete");
                                    echo "\">
                                        <img src=\"";
                                    // line 91
                                    echo Template::get_icon_path("delete.png");
                                    echo "\" alt=\"";
                                    echo get_lang("Delete");
                                    echo "\">
                                    </a>
                                ";
                                }
                                // line 94
                                echo "                            ";
                            }
                            // line 95
                            echo "                        </h3>
                    ";
                        } elseif ( !twig_test_empty($this->getAttribute(                        // line 96
$context["lp_data"], "lp_list", array()))) {
                            // line 97
                            echo "                        <h3 class=\"page-header\">";
                            echo $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getName", array(), "method");
                            echo "</h3>
                    ";
                        }
                        // line 99
                        echo "                ";
                    }
                    // line 100
                    echo "
                ";
                    // line 101
                    if ($this->getAttribute($context["lp_data"], "lp_list", array())) {
                        // line 102
                        echo "                    <div class=\"table-responsive\">
                        <table class=\"table table-hover table-striped\">
                            <thead>
                            <tr>
                                <th>";
                        // line 106
                        echo get_lang("Title");
                        echo "</th>
                                ";
                        // line 107
                        if (($context["is_allowed_to_edit"] ?? null)) {
                            // line 108
                            echo "                                    <th>";
                            echo get_lang("PublicationDate");
                            echo "</th>
                                    <th>";
                            // line 109
                            echo get_lang("ExpirationDate");
                            echo "</th>
                                    <th>";
                            // line 110
                            echo get_lang("Progress");
                            echo "</th>
                                    <th>";
                            // line 111
                            echo get_lang("AuthoringOptions");
                            echo "</th>
                                ";
                        } else {
                            // line 113
                            echo "                                    ";
                            if ( !($context["is_invitee"] ?? null)) {
                                // line 114
                                echo "                                        <th>";
                                echo get_lang("Progress");
                                echo "</th>
                                    ";
                            }
                            // line 116
                            echo "                                    <th>";
                            echo get_lang("Actions");
                            echo "</th>
                                ";
                        }
                        // line 118
                        echo "                            </tr>
                            </thead>
                            <tbody>
                            ";
                        // line 121
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["lp_data"], "lp_list", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                            // line 122
                            echo "                                <tr>
                                    <td>
                                        ";
                            // line 124
                            echo $this->getAttribute($context["row"], "learnpath_icon", array());
                            echo "
                                        <a href=\"";
                            // line 125
                            echo $this->getAttribute($context["row"], "url_start", array());
                            echo "\">
                                            ";
                            // line 126
                            echo $this->getAttribute($context["row"], "title", array());
                            echo "
                                            ";
                            // line 127
                            echo $this->getAttribute($context["row"], "session_image", array());
                            echo "
                                            ";
                            // line 128
                            echo $this->getAttribute($context["row"], "extra", array());
                            echo "
                                        </a>
                                    </td>
                                    ";
                            // line 131
                            if (($context["is_allowed_to_edit"] ?? null)) {
                                // line 132
                                echo "                                        <td>
                                            ";
                                // line 133
                                if ($this->getAttribute($context["row"], "start_time", array())) {
                                    // line 134
                                    echo "                                                <span class=\"small\">";
                                    echo $this->getAttribute($context["row"], "start_time", array());
                                    echo "</span>
                                            ";
                                }
                                // line 136
                                echo "                                        </td>
                                        <td>
                                            <span class=\"small\">";
                                // line 138
                                echo $this->getAttribute($context["row"], "end_time", array());
                                echo "</span>
                                        </td>
                                        <td>
                                            ";
                                // line 141
                                echo $this->getAttribute($context["row"], "dsp_progress", array());
                                echo "
                                        </td>
                                    ";
                            } else {
                                // line 144
                                echo "                                        ";
                                if ( !($context["is_invitee"] ?? null)) {
                                    // line 145
                                    echo "                                            <td>
                                                ";
                                    // line 146
                                    echo $this->getAttribute($context["row"], "dsp_progress", array());
                                    echo "
                                            </td>
                                        ";
                                }
                                // line 149
                                echo "                                    ";
                            }
                            // line 150
                            echo "                                    <td>
                                        ";
                            // line 151
                            echo $this->getAttribute($context["row"], "action_build", array());
                            echo "
                                        ";
                            // line 152
                            echo $this->getAttribute($context["row"], "action_edit", array());
                            echo "
                                        ";
                            // line 153
                            echo $this->getAttribute($context["row"], "action_visible", array());
                            echo "
                                        ";
                            // line 154
                            echo $this->getAttribute($context["row"], "action_tracking", array());
                            echo "
                                        ";
                            // line 155
                            echo $this->getAttribute($context["row"], "action_publish", array());
                            echo "
                                        ";
                            // line 156
                            echo $this->getAttribute($context["row"], "action_subscribe_users", array());
                            echo "
                                        ";
                            // line 157
                            echo $this->getAttribute($context["row"], "action_serious_game", array());
                            echo "
                                        ";
                            // line 158
                            echo $this->getAttribute($context["row"], "action_reinit", array());
                            echo "
                                        ";
                            // line 159
                            echo $this->getAttribute($context["row"], "action_default_view", array());
                            echo "
                                        ";
                            // line 160
                            echo $this->getAttribute($context["row"], "action_debug", array());
                            echo "
                                        ";
                            // line 161
                            echo $this->getAttribute($context["row"], "action_export", array());
                            echo "
                                        ";
                            // line 162
                            echo $this->getAttribute($context["row"], "action_copy", array());
                            echo "
                                        ";
                            // line 163
                            echo $this->getAttribute($context["row"], "action_auto_launch", array());
                            echo "
                                        ";
                            // line 164
                            echo $this->getAttribute($context["row"], "action_pdf", array());
                            echo "
                                        ";
                            // line 165
                            echo $this->getAttribute($context["row"], "action_delete", array());
                            echo "
                                        ";
                            // line 166
                            echo $this->getAttribute($context["row"], "action_order", array());
                            echo "
                                        ";
                            // line 167
                            echo $this->getAttribute($context["row"], "action_update_scorm", array());
                            echo "
                                        ";
                            // line 168
                            echo $this->getAttribute($context["row"], "action_export_to_course_build", array());
                            echo "
                                    </td>
                                </tr>
                            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 172
                        echo "                            </tbody>
                        </table>
                    </div>
                ";
                    }
                    // line 176
                    echo "                <!--- end old view -->
            ";
                } else {
                    // line 178
                    echo "                <!-- new view block accordeon -->
                ";
                    // line 179
                    if (($this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "id", array()) == 0)) {
                        // line 180
                        echo "                    ";
                        if (($context["is_allowed_to_edit"] ?? null)) {
                            // line 181
                            echo "                        ";
                            if ($this->getAttribute($context["lp_data"], "lp_list", array())) {
                                // line 182
                                echo "                            <div class=\"table-responsive\">
                                <table class=\"table table-hover table-striped\">
                                    <thead>
                                    <tr>
                                        <th>";
                                // line 186
                                echo get_lang("Title");
                                echo "</th>
                                        ";
                                // line 187
                                if (($context["is_allowed_to_edit"] ?? null)) {
                                    // line 188
                                    echo "                                            <th>";
                                    echo get_lang("PublicationDate");
                                    echo "</th>
                                            <th>";
                                    // line 189
                                    echo get_lang("ExpirationDate");
                                    echo "</th>
                                            <th>";
                                    // line 190
                                    echo get_lang("Progress");
                                    echo "</th>
                                            <th>";
                                    // line 191
                                    echo get_lang("AuthoringOptions");
                                    echo "</th>
                                        ";
                                } else {
                                    // line 193
                                    echo "                                            ";
                                    if ( !($context["is_invitee"] ?? null)) {
                                        // line 194
                                        echo "                                                <th>";
                                        echo get_lang("Progress");
                                        echo "</th>
                                            ";
                                    }
                                    // line 196
                                    echo "
                                            <th>";
                                    // line 197
                                    echo get_lang("Actions");
                                    echo "</th>
                                        ";
                                }
                                // line 199
                                echo "                                    </tr>
                                    </thead>
                                    <tbody>
                                    ";
                                // line 202
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["lp_data"], "lp_list", array()));
                                foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                                    // line 203
                                    echo "                                        <tr>
                                            <td>
                                                ";
                                    // line 205
                                    echo $this->getAttribute($context["row"], "learnpath_icon", array());
                                    echo "
                                                <a href=\"";
                                    // line 206
                                    echo $this->getAttribute($context["row"], "url_start", array());
                                    echo "\">
                                                    ";
                                    // line 207
                                    echo $this->getAttribute($context["row"], "title", array());
                                    echo "
                                                    ";
                                    // line 208
                                    echo $this->getAttribute($context["row"], "session_image", array());
                                    echo "
                                                    ";
                                    // line 209
                                    echo $this->getAttribute($context["row"], "extra", array());
                                    echo "
                                                </a>
                                            </td>
                                            ";
                                    // line 212
                                    if (($context["is_allowed_to_edit"] ?? null)) {
                                        // line 213
                                        echo "                                                <td>
                                                    ";
                                        // line 214
                                        if ($this->getAttribute($context["row"], "start_time", array())) {
                                            // line 215
                                            echo "                                                        <span class=\"small\">";
                                            echo $this->getAttribute($context["row"], "start_time", array());
                                            echo "</span>
                                                    ";
                                        }
                                        // line 217
                                        echo "                                                </td>
                                                <td>
                                                    <span class=\"small\">";
                                        // line 219
                                        echo $this->getAttribute($context["row"], "end_time", array());
                                        echo "</span>
                                                </td>
                                                <td>
                                                    ";
                                        // line 222
                                        echo $this->getAttribute($context["row"], "dsp_progress", array());
                                        echo "
                                                </td>
                                            ";
                                    } else {
                                        // line 225
                                        echo "                                                ";
                                        if ( !($context["is_invitee"] ?? null)) {
                                            // line 226
                                            echo "                                                    <td>
                                                        ";
                                            // line 227
                                            echo $this->getAttribute($context["row"], "dsp_progress", array());
                                            echo "
                                                    </td>
                                                ";
                                        }
                                        // line 230
                                        echo "                                            ";
                                    }
                                    // line 231
                                    echo "
                                            <td>
                                                ";
                                    // line 233
                                    echo $this->getAttribute($context["row"], "action_build", array());
                                    echo "
                                                ";
                                    // line 234
                                    echo $this->getAttribute($context["row"], "action_edit", array());
                                    echo "
                                                ";
                                    // line 235
                                    echo $this->getAttribute($context["row"], "action_visible", array());
                                    echo "
                                                ";
                                    // line 236
                                    echo $this->getAttribute($context["row"], "action_tracking", array());
                                    echo "
                                                ";
                                    // line 237
                                    echo $this->getAttribute($context["row"], "action_publish", array());
                                    echo "
                                                ";
                                    // line 238
                                    echo $this->getAttribute($context["row"], "action_subscribe_users", array());
                                    echo "
                                                ";
                                    // line 239
                                    echo $this->getAttribute($context["row"], "action_serious_game", array());
                                    echo "
                                                ";
                                    // line 240
                                    echo $this->getAttribute($context["row"], "action_reinit", array());
                                    echo "
                                                ";
                                    // line 241
                                    echo $this->getAttribute($context["row"], "action_default_view", array());
                                    echo "
                                                ";
                                    // line 242
                                    echo $this->getAttribute($context["row"], "action_debug", array());
                                    echo "
                                                ";
                                    // line 243
                                    echo $this->getAttribute($context["row"], "action_export", array());
                                    echo "
                                                ";
                                    // line 244
                                    echo $this->getAttribute($context["row"], "action_copy", array());
                                    echo "
                                                ";
                                    // line 245
                                    echo $this->getAttribute($context["row"], "action_auto_launch", array());
                                    echo "
                                                ";
                                    // line 246
                                    echo $this->getAttribute($context["row"], "action_pdf", array());
                                    echo "
                                                ";
                                    // line 247
                                    echo $this->getAttribute($context["row"], "action_delete", array());
                                    echo "
                                                ";
                                    // line 248
                                    echo $this->getAttribute($context["row"], "action_order", array());
                                    echo "
                                                ";
                                    // line 249
                                    echo $this->getAttribute($context["row"], "action_update_scorm", array());
                                    echo "
                                            </td>
                                        </tr>
                                    ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 253
                                echo "                                    </tbody>
                                </table>
                            </div>
                        ";
                            }
                            // line 257
                            echo "                    ";
                        } else {
                            // line 258
                            echo "                        <div id=\"not-category\" class=\"panel panel-default\">
                            <div class=\"panel-body\">
                                ";
                            // line 260
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["lp_data"], "lp_list", array()));
                            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                                // line 261
                                echo "                                    <div class=\"lp-item\">
                                        <div class=\"row\">
                                            <div class=\"col-md-8\">
                                                <i class=\"fa fa-chevron-circle-right\" aria-hidden=\"true\"></i>
                                                <a href=\"";
                                // line 265
                                echo $this->getAttribute($context["row"], "url_start", array());
                                echo "\">
                                                    ";
                                // line 266
                                echo $this->getAttribute($context["row"], "title", array());
                                echo "
                                                    ";
                                // line 267
                                echo $this->getAttribute($context["row"], "session_image", array());
                                echo "
                                                    ";
                                // line 268
                                echo $this->getAttribute($context["row"], "extra", array());
                                echo "
                                                </a>
                                            </div>
                                            <div class=\"col-md-3\">
                                                ";
                                // line 272
                                echo $this->getAttribute($context["row"], "dsp_progress", array());
                                echo "
                                            </div>
                                            <div class=\"col-md-1\">
                                                ";
                                // line 275
                                echo $this->getAttribute($context["row"], "action_pdf", array());
                                echo "
                                            </div>
                                        </div>
                                    </div>
                                ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 280
                            echo "                            </div>
                        </div>
                    ";
                        }
                        // line 283
                        echo "                ";
                    }
                    // line 284
                    echo "
                ";
                    // line 285
                    if (((twig_length_filter($this->env, ($context["categories"] ?? null)) > 1) && $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "id", array()))) {
                        // line 286
                        echo "                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\" role=\"tab\" id=\"heading-";
                        // line 287
                        echo $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method");
                        echo "\">
                            ";
                        // line 288
                        if (($context["is_allowed_to_edit"] ?? null)) {
                            // line 289
                            echo "                                <div class=\"tools-actions pull-right\">
                                    ";
                            // line 290
                            if (($this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method") > 0)) {
                                // line 291
                                echo "                                        ";
                                if ( !$this->getAttribute(($context["_c"] ?? null), "session_id", array())) {
                                    // line 292
                                    echo "                                            <a href=\"";
                                    echo ((("lp_controller.php?" . $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array())) . "&action=add_lp_category&id=") . $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method"));
                                    echo "\"
                                               title=\"";
                                    // line 293
                                    echo get_lang("Edit");
                                    echo "\">
                                                <img src=\"";
                                    // line 294
                                    echo Template::get_icon_path("edit.png");
                                    echo "\" alt=\"";
                                    echo get_lang("Edit");
                                    echo "\">
                                            </a>

                                            ";
                                    // line 297
                                    if ($this->getAttribute(($context["subscription_settings"] ?? null), "allow_add_users_to_lp_category", array())) {
                                        // line 298
                                        echo "                                                <a href=\"";
                                        echo ((("lp_controller.php?" . $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array())) . "&action=add_users_to_category&id=") . $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method"));
                                        echo "\"
                                                   title=\"";
                                        // line 299
                                        echo get_lang("AddUsers");
                                        echo "\">
                                                    <img src=\"";
                                        // line 300
                                        echo Template::get_icon_path("user.png");
                                        echo "\" alt=\"";
                                        echo get_lang("AddUsers");
                                        echo "\">
                                                </a>
                                            ";
                                    }
                                    // line 303
                                    echo "
                                            ";
                                    // line 304
                                    if (($this->getAttribute($context["loop"], "index0", array()) == 1)) {
                                        // line 305
                                        echo "                                                <a href=\"#\">
                                                    <img src=\"";
                                        // line 306
                                        echo Template::get_icon_path("up_na.png");
                                        echo "\" alt=\"";
                                        echo get_lang("Move");
                                        echo "\">
                                                </a>
                                            ";
                                    } else {
                                        // line 309
                                        echo "                                                <a href=\"";
                                        echo ((("lp_controller.php?" . $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array())) . "&action=move_up_category&id=") . $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method"));
                                        echo "\"
                                                   title=\"";
                                        // line 310
                                        echo get_lang("Move");
                                        echo "\">
                                                    <img src=\"";
                                        // line 311
                                        echo Template::get_icon_path("up.png");
                                        echo "\" alt=\"";
                                        echo get_lang("Move");
                                        echo "\">
                                                </a>
                                            ";
                                    }
                                    // line 314
                                    echo "
                                            ";
                                    // line 315
                                    if (((twig_length_filter($this->env, ($context["data"] ?? null)) - 1) == $this->getAttribute($context["loop"], "index0", array()))) {
                                        // line 316
                                        echo "                                                <a href=\"#\">
                                                    <img src=\"";
                                        // line 317
                                        echo Template::get_icon_path("down_na.png");
                                        echo "\" alt=\"";
                                        echo get_lang("Move");
                                        echo "\">
                                                </a>
                                            ";
                                    } else {
                                        // line 320
                                        echo "                                                <a href=\"";
                                        echo ((("lp_controller.php?" . $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array())) . "&action=move_down_category&id=") . $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method"));
                                        echo "\"
                                                   title=\"";
                                        // line 321
                                        echo get_lang("Move");
                                        echo "\">
                                                    <img src=\"";
                                        // line 322
                                        echo Template::get_icon_path("down.png");
                                        echo "\" alt=\"";
                                        echo get_lang("Move");
                                        echo "\">
                                                </a>
                                            ";
                                    }
                                    // line 325
                                    echo "                                        ";
                                }
                                // line 326
                                echo "
                                        ";
                                // line 327
                                if (($this->getAttribute($context["lp_data"], "category_visibility", array()) == 0)) {
                                    // line 328
                                    echo "                                            <a href=\"lp_controller.php?";
                                    echo (($this->getAttribute(($context["_p"] ?? null), "web_cid_query", array()) . "&") . twig_urlencode_filter(array("action" => "toggle_category_visibility", "id" => $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "id", array()), "new_status" => 1)));
                                    echo "\"
                                               title=\"";
                                    // line 329
                                    echo get_lang("Show");
                                    echo "\">
                                                <img src=\"";
                                    // line 330
                                    echo Template::get_icon_path("invisible.png");
                                    echo "\" alt=\"";
                                    echo get_lang("Show");
                                    echo "\">
                                            </a>
                                        ";
                                } else {
                                    // line 333
                                    echo "                                            <a href=\"lp_controller.php?";
                                    echo (($this->getAttribute(($context["_p"] ?? null), "web_cid_query", array()) . "&") . twig_urlencode_filter(array("action" => "toggle_category_visibility", "id" => $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "id", array()), "new_status" => 0)));
                                    echo "\"
                                               title=\"";
                                    // line 334
                                    echo get_lang("Hide");
                                    echo "\">
                                                <img src=\"";
                                    // line 335
                                    echo Template::get_icon_path("visible.png");
                                    echo "\" alt=\"";
                                    echo get_lang("Hide");
                                    echo "\">
                                            </a>
                                        ";
                                }
                                // line 338
                                echo "
                                        ";
                                // line 339
                                if (($this->getAttribute($context["lp_data"], "category_visibility", array()) == 1)) {
                                    // line 340
                                    echo "                                            ";
                                    if (($this->getAttribute($context["lp_data"], "category_is_published", array()) == 0)) {
                                        // line 341
                                        echo "                                                <a href=\"lp_controller.php?";
                                        echo (($this->getAttribute(($context["_p"] ?? null), "web_cid_query", array()) . "&") . twig_urlencode_filter(array("action" => "toggle_category_publish", "id" => $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "id", array()), "new_status" => 1)));
                                        echo "\"
                                                   title=\"";
                                        // line 342
                                        echo get_lang("LearnpathPublish");
                                        echo "\">
                                                    <img src=\"";
                                        // line 343
                                        echo Template::get_icon_path("lp_publish_na.png");
                                        echo "\"
                                                         alt=\"";
                                        // line 344
                                        echo get_lang("LearnpathPublish");
                                        echo "\">
                                                </a>
                                            ";
                                    } else {
                                        // line 347
                                        echo "                                                <a href=\"lp_controller.php?";
                                        echo (($this->getAttribute(($context["_p"] ?? null), "web_cid_query", array()) . "&") . twig_urlencode_filter(array("action" => "toggle_category_publish", "id" => $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "id", array()), "new_status" => 0)));
                                        echo "\"
                                                   title=\"";
                                        // line 348
                                        echo get_lang("LearnpathDoNotPublish");
                                        echo "\">
                                                    <img src=\"";
                                        // line 349
                                        echo Template::get_icon_path("lp_publish.png");
                                        echo "\" alt=\"";
                                        echo get_lang("Hide");
                                        echo "\">
                                                </a>
                                            ";
                                    }
                                    // line 352
                                    echo "                                        ";
                                } else {
                                    // line 353
                                    echo "                                            <img src=\"";
                                    echo Template::get_icon_path("lp_publish_na.png");
                                    echo "\"
                                                 alt=\"";
                                    // line 354
                                    echo get_lang("LearnpathPublish");
                                    echo "\">
                                        ";
                                }
                                // line 356
                                echo "
                                        ";
                                // line 357
                                if ( !$this->getAttribute(($context["_c"] ?? null), "session_id", array())) {
                                    // line 358
                                    echo "                                            <a href=\"";
                                    echo ((("lp_controller.php?" . $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array())) . "&action=delete_lp_category&id=") . $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method"));
                                    echo "\"
                                               title=\"";
                                    // line 359
                                    echo get_lang("Delete");
                                    echo "\">
                                                <img src=\"";
                                    // line 360
                                    echo Template::get_icon_path("delete.png");
                                    echo "\" alt=\"";
                                    echo get_lang("Delete");
                                    echo "\">
                                            </a>
                                        ";
                                }
                                // line 363
                                echo "                                    ";
                            }
                            // line 364
                            echo "                                </div>
                            ";
                        }
                        // line 366
                        echo "                            <h4 class=\"panel-title\">
                                <a role=\"button\" data-toggle=\"collapse\" data-parent=\"#lp-accordion\"
                                   href=\"#collapse-";
                        // line 368
                        echo $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method");
                        echo "\" aria-expanded=\"true\"
                                   aria-controls=\"collapse-";
                        // line 369
                        echo $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method");
                        echo "\">
                                    ";
                        // line 370
                        echo $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getName", array(), "method");
                        echo "
                                </a>
                            </h4>
                        </div>

                        <div id=\"collapse-";
                        // line 375
                        echo $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method");
                        echo "\" class=\"panel-collapse collapse ";
                        echo (((twig_length_filter($this->env, ($context["categories"] ?? null)) > 1)) ? ("in") : (""));
                        echo "\"
                             role=\"tabpanel\" aria-labelledby=\"heading-";
                        // line 376
                        echo $this->getAttribute($this->getAttribute($context["lp_data"], "category", array()), "getId", array(), "method");
                        echo "\">
                            <div class=\"panel-body\">
                                ";
                        // line 378
                        if ($this->getAttribute($context["lp_data"], "lp_list", array())) {
                            // line 379
                            echo "
                                    ";
                            // line 380
                            if (($context["is_allowed_to_edit"] ?? null)) {
                                // line 381
                                echo "                                        <div class=\"table-responsive\">
                                            <table class=\"table table-hover table-striped\">
                                                <thead>
                                                <tr>
                                                    <th>";
                                // line 385
                                echo get_lang("Title");
                                echo "</th>
                                                    ";
                                // line 386
                                if (($context["is_allowed_to_edit"] ?? null)) {
                                    // line 387
                                    echo "                                                        <th>";
                                    echo get_lang("PublicationDate");
                                    echo "</th>
                                                        <th>";
                                    // line 388
                                    echo get_lang("ExpirationDate");
                                    echo "</th>
                                                        <th>";
                                    // line 389
                                    echo get_lang("Progress");
                                    echo "</th>
                                                        <th>";
                                    // line 390
                                    echo get_lang("AuthoringOptions");
                                    echo "</th>
                                                    ";
                                } else {
                                    // line 392
                                    echo "                                                        ";
                                    if ( !($context["is_invitee"] ?? null)) {
                                        // line 393
                                        echo "                                                            <th>";
                                        echo get_lang("Progress");
                                        echo "</th>
                                                        ";
                                    }
                                    // line 395
                                    echo "
                                                        <th>";
                                    // line 396
                                    echo get_lang("Actions");
                                    echo "</th>
                                                    ";
                                }
                                // line 398
                                echo "                                                </tr>
                                                </thead>
                                                <tbody>
                                                ";
                                // line 401
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["lp_data"], "lp_list", array()));
                                foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                                    // line 402
                                    echo "                                                    <tr>
                                                        <td>
                                                            ";
                                    // line 404
                                    echo $this->getAttribute($context["row"], "learnpath_icon", array());
                                    echo "
                                                            <a href=\"";
                                    // line 405
                                    echo $this->getAttribute($context["row"], "url_start", array());
                                    echo "\">
                                                                ";
                                    // line 406
                                    echo $this->getAttribute($context["row"], "title", array());
                                    echo "
                                                                ";
                                    // line 407
                                    echo $this->getAttribute($context["row"], "session_image", array());
                                    echo "
                                                                ";
                                    // line 408
                                    echo $this->getAttribute($context["row"], "extra", array());
                                    echo "
                                                            </a>
                                                        </td>
                                                        ";
                                    // line 411
                                    if (($context["is_allowed_to_edit"] ?? null)) {
                                        // line 412
                                        echo "                                                            <td>
                                                                ";
                                        // line 413
                                        if ($this->getAttribute($context["row"], "start_time", array())) {
                                            // line 414
                                            echo "                                                                    <span class=\"small\">";
                                            echo $this->getAttribute($context["row"], "start_time", array());
                                            echo "</span>
                                                                ";
                                        }
                                        // line 416
                                        echo "                                                            </td>
                                                            <td>
                                                                <span class=\"small\">";
                                        // line 418
                                        echo $this->getAttribute($context["row"], "end_time", array());
                                        echo "</span>
                                                            </td>
                                                            <td>
                                                                ";
                                        // line 421
                                        echo $this->getAttribute($context["row"], "dsp_progress", array());
                                        echo "
                                                            </td>
                                                        ";
                                    } else {
                                        // line 424
                                        echo "                                                            ";
                                        if ( !($context["is_invitee"] ?? null)) {
                                            // line 425
                                            echo "                                                                <td>
                                                                    ";
                                            // line 426
                                            echo $this->getAttribute($context["row"], "dsp_progress", array());
                                            echo "
                                                                </td>
                                                            ";
                                        }
                                        // line 429
                                        echo "                                                        ";
                                    }
                                    // line 430
                                    echo "
                                                        <td>
                                                            ";
                                    // line 432
                                    echo $this->getAttribute($context["row"], "action_build", array());
                                    echo "
                                                            ";
                                    // line 433
                                    echo $this->getAttribute($context["row"], "action_edit", array());
                                    echo "
                                                            ";
                                    // line 434
                                    echo $this->getAttribute($context["row"], "action_visible", array());
                                    echo "
                                                            ";
                                    // line 435
                                    echo $this->getAttribute($context["row"], "action_tracking", array());
                                    echo "
                                                            ";
                                    // line 436
                                    echo $this->getAttribute($context["row"], "action_publish", array());
                                    echo "
                                                            ";
                                    // line 437
                                    echo $this->getAttribute($context["row"], "action_subscribe_users", array());
                                    echo "
                                                            ";
                                    // line 438
                                    echo $this->getAttribute($context["row"], "action_serious_game", array());
                                    echo "
                                                            ";
                                    // line 439
                                    echo $this->getAttribute($context["row"], "action_reinit", array());
                                    echo "
                                                            ";
                                    // line 440
                                    echo $this->getAttribute($context["row"], "action_default_view", array());
                                    echo "
                                                            ";
                                    // line 441
                                    echo $this->getAttribute($context["row"], "action_debug", array());
                                    echo "
                                                            ";
                                    // line 442
                                    echo $this->getAttribute($context["row"], "action_export", array());
                                    echo "
                                                            ";
                                    // line 443
                                    echo $this->getAttribute($context["row"], "action_copy", array());
                                    echo "
                                                            ";
                                    // line 444
                                    echo $this->getAttribute($context["row"], "action_auto_launch", array());
                                    echo "
                                                            ";
                                    // line 445
                                    echo $this->getAttribute($context["row"], "action_pdf", array());
                                    echo "
                                                            ";
                                    // line 446
                                    echo $this->getAttribute($context["row"], "action_delete", array());
                                    echo "
                                                            ";
                                    // line 447
                                    echo $this->getAttribute($context["row"], "action_order", array());
                                    echo "
                                                            ";
                                    // line 448
                                    echo $this->getAttribute($context["row"], "action_update_scorm", array());
                                    echo "
                                                        </td>
                                                    </tr>
                                                ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 452
                                echo "                                                </tbody>
                                            </table>
                                        </div>
                                    ";
                            } else {
                                // line 456
                                echo "                                        ";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["lp_data"], "lp_list", array()));
                                foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                                    // line 457
                                    echo "                                            <div class=\"lp-item\">
                                                <div class=\"row\">
                                                    <div class=\"col-md-8\">
                                                        <i class=\"fa fa-chevron-circle-right\" aria-hidden=\"true\"></i>
                                                        <a href=\"";
                                    // line 461
                                    echo $this->getAttribute($context["row"], "url_start", array());
                                    echo "\">
                                                            ";
                                    // line 462
                                    echo $this->getAttribute($context["row"], "title", array());
                                    echo "
                                                            ";
                                    // line 463
                                    echo $this->getAttribute($context["row"], "session_image", array());
                                    echo "
                                                            ";
                                    // line 464
                                    echo $this->getAttribute($context["row"], "extra", array());
                                    echo "
                                                        </a>
                                                    </div>
                                                    <div class=\"col-md-3\">
                                                        ";
                                    // line 468
                                    echo $this->getAttribute($context["row"], "dsp_progress", array());
                                    echo "
                                                    </div>
                                                    <div class=\"col-md-1\">
                                                        ";
                                    // line 471
                                    echo $this->getAttribute($context["row"], "action_pdf", array());
                                    echo "
                                                    </div>
                                                </div>
                                            </div>
                                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 476
                                echo "                                    ";
                            }
                            // line 477
                            echo "                                ";
                        }
                        // line 478
                        echo "                            </div>
                        </div>
                    </div>

                ";
                    }
                    // line 483
                    echo "                <!-- end view block accordeon -->
            ";
                }
                // line 485
                echo "        ";
            }
            // line 486
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lp_data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 487
        echo "</div>
";
        // line 488
        if ((($context["is_allowed_to_edit"] ?? null) &&  !($context["lp_is_shown"] ?? null))) {
            // line 489
            echo "    <div id=\"no-data-view\">
        <h2>";
            // line 490
            echo get_lang("LearningPaths");
            echo "</h2>
        <img src=\"";
            // line 491
            echo Template::get_icon_path("scorms.png", 64);
            echo "\" width=\"64\" height=\"64\">
        <div class=\"controls\">
            <a href=\"";
            // line 493
            echo (((($context["web_self"] ?? null) . "?") . $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array())) . "&action=add_lp");
            echo "\" class=\"btn btn-default\">
                ";
            // line 494
            echo get_lang("LearnpathAddLearnpath");
            echo "
            </a>
        </div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "default/learnpath/list.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1424 => 494,  1420 => 493,  1415 => 491,  1411 => 490,  1408 => 489,  1406 => 488,  1403 => 487,  1389 => 486,  1386 => 485,  1382 => 483,  1375 => 478,  1372 => 477,  1369 => 476,  1358 => 471,  1352 => 468,  1345 => 464,  1341 => 463,  1337 => 462,  1333 => 461,  1327 => 457,  1322 => 456,  1316 => 452,  1306 => 448,  1302 => 447,  1298 => 446,  1294 => 445,  1290 => 444,  1286 => 443,  1282 => 442,  1278 => 441,  1274 => 440,  1270 => 439,  1266 => 438,  1262 => 437,  1258 => 436,  1254 => 435,  1250 => 434,  1246 => 433,  1242 => 432,  1238 => 430,  1235 => 429,  1229 => 426,  1226 => 425,  1223 => 424,  1217 => 421,  1211 => 418,  1207 => 416,  1201 => 414,  1199 => 413,  1196 => 412,  1194 => 411,  1188 => 408,  1184 => 407,  1180 => 406,  1176 => 405,  1172 => 404,  1168 => 402,  1164 => 401,  1159 => 398,  1154 => 396,  1151 => 395,  1145 => 393,  1142 => 392,  1137 => 390,  1133 => 389,  1129 => 388,  1124 => 387,  1122 => 386,  1118 => 385,  1112 => 381,  1110 => 380,  1107 => 379,  1105 => 378,  1100 => 376,  1094 => 375,  1086 => 370,  1082 => 369,  1078 => 368,  1074 => 366,  1070 => 364,  1067 => 363,  1059 => 360,  1055 => 359,  1050 => 358,  1048 => 357,  1045 => 356,  1040 => 354,  1035 => 353,  1032 => 352,  1024 => 349,  1020 => 348,  1015 => 347,  1009 => 344,  1005 => 343,  1001 => 342,  996 => 341,  993 => 340,  991 => 339,  988 => 338,  980 => 335,  976 => 334,  971 => 333,  963 => 330,  959 => 329,  954 => 328,  952 => 327,  949 => 326,  946 => 325,  938 => 322,  934 => 321,  929 => 320,  921 => 317,  918 => 316,  916 => 315,  913 => 314,  905 => 311,  901 => 310,  896 => 309,  888 => 306,  885 => 305,  883 => 304,  880 => 303,  872 => 300,  868 => 299,  863 => 298,  861 => 297,  853 => 294,  849 => 293,  844 => 292,  841 => 291,  839 => 290,  836 => 289,  834 => 288,  830 => 287,  827 => 286,  825 => 285,  822 => 284,  819 => 283,  814 => 280,  803 => 275,  797 => 272,  790 => 268,  786 => 267,  782 => 266,  778 => 265,  772 => 261,  768 => 260,  764 => 258,  761 => 257,  755 => 253,  745 => 249,  741 => 248,  737 => 247,  733 => 246,  729 => 245,  725 => 244,  721 => 243,  717 => 242,  713 => 241,  709 => 240,  705 => 239,  701 => 238,  697 => 237,  693 => 236,  689 => 235,  685 => 234,  681 => 233,  677 => 231,  674 => 230,  668 => 227,  665 => 226,  662 => 225,  656 => 222,  650 => 219,  646 => 217,  640 => 215,  638 => 214,  635 => 213,  633 => 212,  627 => 209,  623 => 208,  619 => 207,  615 => 206,  611 => 205,  607 => 203,  603 => 202,  598 => 199,  593 => 197,  590 => 196,  584 => 194,  581 => 193,  576 => 191,  572 => 190,  568 => 189,  563 => 188,  561 => 187,  557 => 186,  551 => 182,  548 => 181,  545 => 180,  543 => 179,  540 => 178,  536 => 176,  530 => 172,  520 => 168,  516 => 167,  512 => 166,  508 => 165,  504 => 164,  500 => 163,  496 => 162,  492 => 161,  488 => 160,  484 => 159,  480 => 158,  476 => 157,  472 => 156,  468 => 155,  464 => 154,  460 => 153,  456 => 152,  452 => 151,  449 => 150,  446 => 149,  440 => 146,  437 => 145,  434 => 144,  428 => 141,  422 => 138,  418 => 136,  412 => 134,  410 => 133,  407 => 132,  405 => 131,  399 => 128,  395 => 127,  391 => 126,  387 => 125,  383 => 124,  379 => 122,  375 => 121,  370 => 118,  364 => 116,  358 => 114,  355 => 113,  350 => 111,  346 => 110,  342 => 109,  337 => 108,  335 => 107,  331 => 106,  325 => 102,  323 => 101,  320 => 100,  317 => 99,  311 => 97,  309 => 96,  306 => 95,  303 => 94,  295 => 91,  291 => 90,  286 => 89,  283 => 88,  275 => 85,  271 => 84,  266 => 83,  260 => 80,  256 => 79,  252 => 78,  247 => 77,  245 => 76,  242 => 75,  234 => 72,  230 => 71,  225 => 70,  217 => 67,  213 => 66,  208 => 65,  206 => 64,  203 => 63,  200 => 62,  192 => 59,  188 => 58,  183 => 57,  175 => 54,  172 => 53,  170 => 52,  167 => 51,  159 => 48,  155 => 47,  150 => 46,  142 => 43,  139 => 42,  137 => 41,  134 => 40,  126 => 37,  122 => 36,  117 => 35,  115 => 34,  107 => 31,  103 => 30,  98 => 29,  95 => 28,  93 => 27,  88 => 25,  85 => 24,  82 => 23,  80 => 22,  77 => 21,  74 => 20,  72 => 19,  69 => 18,  66 => 17,  63 => 16,  61 => 15,  58 => 14,  55 => 13,  38 => 12,  35 => 11,  33 => 10,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/learnpath/list.tpl", "/var/www/mylms/main/template/default/learnpath/list.tpl");
    }
}
