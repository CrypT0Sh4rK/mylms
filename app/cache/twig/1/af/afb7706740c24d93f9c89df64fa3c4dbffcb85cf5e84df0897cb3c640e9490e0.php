<?php

/* default/my_space/accessoverview.tpl */
class __TwigTemplate_852714bd44c64afd84c990dba1be760b9caf6ff47ddb4ef56e39d5c8fac83087 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h2 class=\"page-header\">";
        echo get_lang("DisplayAccessOverview");
        echo "</h2>

";
        // line 3
        echo ($context["form"] ?? null);
        echo "

<h3 class=\"page-header\">";
        // line 5
        echo get_lang("Results");
        echo "</h3>

";
        // line 7
        echo ($context["table"] ?? null);
        echo "

<script>
    \$(document).on('ready', function () {
        var courseIdEl = \$('#access_overview_course_id'),
            sessionIdEl = \$('#access_overview_session_id');

        if (!courseIdEl.val()) {
            sessionIdEl
                .prop('disabled', true)
                .selectpicker('refresh');
        }

        courseIdEl.on('change', function() {
            var self = \$(this);

            if (!this.value) {
                sessionIdEl
                    .prop(\"disabled\", true)
                    .selectpicker('refresh');

                return;
            }

            sessionIdEl
                .prop(\"disabled\", false)
                .selectpicker('refresh');
        });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "default/my_space/accessoverview.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 7,  30 => 5,  25 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/my_space/accessoverview.tpl", "/var/www/mylms/main/template/default/my_space/accessoverview.tpl");
    }
}
