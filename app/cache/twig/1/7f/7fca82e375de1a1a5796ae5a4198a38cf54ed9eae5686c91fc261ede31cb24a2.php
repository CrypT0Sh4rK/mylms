<?php

/* default/export/pdf_footer.tpl */
class __TwigTemplate_290ae0a7271ff30597fe346255185d6c52a46026ea5dc65c742805b7750f0bfe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table border=\"0\" class=\"full-width border-top page-footer\">
    <tr>
        <td class=\"text-left\">
            <strong>";
        // line 4
        echo $this->getAttribute(($context["_s"] ?? null), "institution", array());
        echo "</strong>
        </td>
        <td class=\"text-right\">
            <strong>{PAGENO} / {nb}</strong>
        </td>
    </tr>
</table>
";
    }

    public function getTemplateName()
    {
        return "default/export/pdf_footer.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 4,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/export/pdf_footer.tpl", "/var/www/mylms/main/template/default/export/pdf_footer.tpl");
    }
}
