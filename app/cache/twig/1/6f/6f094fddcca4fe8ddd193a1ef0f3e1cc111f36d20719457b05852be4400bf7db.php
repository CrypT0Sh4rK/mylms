<?php

/* default/forum/list.tpl */
class __TwigTemplate_8b7c5aa0c9d436972f4c0d9b61934d5096964e8670a8ae53db9cf3132d9e527d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(Template::findTemplateFilePath("layout/layout_1_col.tpl"), "default/forum/list.tpl", 1);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
";
        // line 4
        echo ($context["form_content"] ?? null);
        echo "

";
        // line 6
        if ( !twig_test_empty(($context["data"] ?? null))) {
            // line 7
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["data"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 8
                echo "        <div class=\"category-forum\" id=\"category_";
                echo $this->getAttribute($context["item"], "id", array());
                echo "\">
            <div class=\"pull-right\">
                ";
                // line 10
                echo $this->getAttribute($context["item"], "tools", array());
                echo "
            </div>
            <h3>
                ";
                // line 13
                echo Template::get_image("forum_blue.png", 32);
                echo "
                <a href=\"";
                // line 14
                echo $this->getAttribute($context["item"], "url", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["item"], "title", array());
                echo "\">";
                echo $this->getAttribute($context["item"], "title", array());
                echo $this->getAttribute($context["item"], "icon_session", array());
                echo "</a>
            </h3>
            <div class=\"forum-description\">
                ";
                // line 17
                echo $this->getAttribute($context["item"], "description", array());
                echo "
            </div>
        </div>
            ";
                // line 20
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["item"], "forums", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["subitem"]) {
                    // line 21
                    echo "                <div class=\"forum_display\">
                    <div class=\"panel panel-default forum\">
                        <div class=\"panel-body\">
                            <div class=\"row\">
                                <div class=\"col-md-3\">
                                    <div class=\"number-post\">
                                        <a href=\"";
                    // line 27
                    echo $this->getAttribute(($context["forum"] ?? null), "url", array());
                    echo "\" title=\"";
                    echo $this->getAttribute(($context["forum"] ?? null), "title", array());
                    echo "\">
                                        ";
                    // line 28
                    if ( !twig_test_empty($this->getAttribute($context["subitem"], "forum_image", array()))) {
                        // line 29
                        echo "                                            <img src=\"";
                        echo $this->getAttribute($context["subitem"], "forum_image", array());
                        echo "\" width=\"48px\">
                                        ";
                    } else {
                        // line 31
                        echo "                                            ";
                        if (($this->getAttribute($context["subitem"], "forum_of_group", array()) == 0)) {
                            // line 32
                            echo "                                                ";
                            echo Template::get_image("forum_group.png", 48);
                            echo "
                                            ";
                        } else {
                            // line 34
                            echo "                                                ";
                            echo Template::get_image("forum.png", 48);
                            echo "
                                            ";
                        }
                        // line 36
                        echo "                                        ";
                    }
                    // line 37
                    echo "                                        </a>
                                        <p>";
                    // line 38
                    echo get_lang("ForumThreads");
                    echo ": ";
                    echo $this->getAttribute($context["subitem"], "number_threads", array());
                    echo " </p>
                                    </div>
                                </div>
                                <div class=\"col-md-9\">
                                    <div class=\"pull-right\">
                                        <div class=\"toolbar\">
                                            ";
                    // line 44
                    echo $this->getAttribute($context["subitem"], "tools", array());
                    echo "
                                        </div>
                                    </div>
                                    <h3 class=\"title\">
                                    ";
                    // line 48
                    echo Template::get_image("forum_yellow.png", 32);
                    echo "
                                    <a href=\"";
                    // line 49
                    echo $this->getAttribute($context["subitem"], "url", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["subitem"], "title", array());
                    echo "\" class=\"";
                    echo ((($this->getAttribute($context["subitem"], "visibility", array()) != "1")) ? ("text-muted") : (""));
                    echo "\">";
                    echo $this->getAttribute($context["subitem"], "title", array());
                    echo "</a>
                                    ";
                    // line 50
                    if (($this->getAttribute($context["subitem"], "forum_of_group", array()) != 0)) {
                        // line 51
                        echo "                                        <a class=\"forum-goto\" href=\"../group/group_space.php?";
                        echo $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array());
                        echo "&gidReq=";
                        echo $this->getAttribute($context["subitem"], "forum_of_group", array());
                        echo "\">
                                            ";
                        // line 52
                        echo Template::get_image("forum.png", 22);
                        echo " ";
                        echo get_lang("GoTo");
                        echo " ";
                        echo $this->getAttribute($context["subitem"], "forum_group_title", array());
                        echo "
                                        </a>
                                    ";
                    }
                    // line 55
                    echo "                                    ";
                    echo $this->getAttribute($context["subitem"], "icon_session", array());
                    echo "
                                    </h3>
                                    ";
                    // line 57
                    if ( !twig_test_empty($this->getAttribute($context["subitem"], "last_poster_id", array()))) {
                        // line 58
                        echo "                                        <div class=\"forum-date\">
                                            <i class=\"fa fa-comments\" aria-hidden=\"true\"></i>
                                            ";
                        // line 60
                        echo $this->getAttribute($context["subitem"], "last_poster_date", array());
                        echo "
                                            ";
                        // line 61
                        echo get_lang("By");
                        echo "
                                            ";
                        // line 62
                        echo $this->getAttribute($context["subitem"], "last_poster_user", array());
                        echo "
                                        </div>
                                    ";
                    }
                    // line 65
                    echo "                                    <div class=\"description\">
                                        ";
                    // line 66
                    echo $this->getAttribute($context["subitem"], "description", array());
                    echo "
                                    </div>
                                    ";
                    // line 68
                    echo $this->getAttribute($context["subitem"], "alert", array());
                    echo "
                                    ";
                    // line 69
                    if ( !twig_test_empty($this->getAttribute($context["subitem"], "moderation", array()))) {
                        // line 70
                        echo "                                        <span class=\"label label-warning\">
                                            ";
                        // line 71
                        echo get_lang("PostsPendingModeration");
                        echo ": ";
                        echo $this->getAttribute($context["subitem"], "moderation", array());
                        echo "
                                        </span>
                                    ";
                    }
                    // line 74
                    echo "                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subitem'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 80
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo "    ";
        } else {
            // line 82
            echo "        <div class=\"alert alert-warning\">
            ";
            // line 83
            echo get_lang("NoForumInThisCategory");
            echo "
        </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "default/forum/list.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  250 => 83,  247 => 82,  244 => 81,  238 => 80,  227 => 74,  219 => 71,  216 => 70,  214 => 69,  210 => 68,  205 => 66,  202 => 65,  196 => 62,  192 => 61,  188 => 60,  184 => 58,  182 => 57,  176 => 55,  166 => 52,  159 => 51,  157 => 50,  147 => 49,  143 => 48,  136 => 44,  125 => 38,  122 => 37,  119 => 36,  113 => 34,  107 => 32,  104 => 31,  98 => 29,  96 => 28,  90 => 27,  82 => 21,  78 => 20,  72 => 17,  61 => 14,  57 => 13,  51 => 10,  45 => 8,  40 => 7,  38 => 6,  33 => 4,  30 => 3,  27 => 2,  18 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/forum/list.tpl", "/var/www/mylms/main/template/default/forum/list.tpl");
    }
}
