<?php

/* default/layout/hot_course_item.tpl */
class __TwigTemplate_6a53ed0e7fc70c6141e83e18d6506ed0492c6fc10ae2734a579ef36d784e6972 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["hot_courses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 2
            echo "    ";
            if ($this->getAttribute($context["item"], "title", array())) {
                // line 3
                echo "        <div class=\"col-xs-12 col-sm-6 col-md-4\">
            <div class=\"items items-hotcourse\">
                <div class=\"image\">
                        <a title=\"";
                // line 6
                echo $this->getAttribute($context["item"], "title", array());
                echo "\" href=\"";
                echo $this->getAttribute(($context["_p"] ?? null), "web", array());
                echo "course/";
                echo $this->getAttribute($context["item"], "real_id", array());
                echo "/about\">
                            <img src=\"";
                // line 7
                echo $this->getAttribute($context["item"], "course_image_large", array());
                echo "\" class=\"img-responsive\" alt=\"";
                echo $this->getAttribute($context["item"], "title", array());
                echo "\">
                        </a>

                    ";
                // line 10
                if (($this->getAttribute($context["item"], "categoryName", array()) != "")) {
                    // line 11
                    echo "                        <span class=\"category\">";
                    echo $this->getAttribute($context["item"], "categoryName", array());
                    echo "</span>
                        <div class=\"cribbon\"></div>
                    ";
                }
                // line 14
                echo "                    <div class=\"user-actions\">";
                echo $this->getAttribute($context["item"], "description_button", array());
                echo "</div>
                </div>
                <div class=\"description\">
                    <div class=\"block-title\">
                        <h5 class=\"title\">
                            <a alt=\"";
                // line 19
                echo $this->getAttribute($context["item"], "title", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["item"], "title", array());
                echo "\" href=\"";
                echo $this->getAttribute(($context["_p"] ?? null), "web", array());
                echo "course/";
                echo $this->getAttribute($context["item"], "real_id", array());
                echo "/about\">
                                ";
                // line 20
                echo $this->getAttribute($context["item"], "title_cut", array());
                echo "
                            </a>
                        </h5>
                    </div>
                    <div class=\"ranking\">
                        ";
                // line 25
                echo $this->getAttribute($context["item"], "rating_html", array());
                echo "
                    </div>
                    <div class=\"toolbar row\">
                        <div class=\"col-sm-4\">
                            ";
                // line 29
                if ($this->getAttribute($context["item"], "price", array())) {
                    // line 30
                    echo "                                ";
                    echo $this->getAttribute($context["item"], "price", array());
                    echo "
                            ";
                }
                // line 32
                echo "                        </div>
                        <div class=\"col-sm-8\">
                            <div class=\"btn-group\" role=\"group\">
                                ";
                // line 35
                echo $this->getAttribute($context["item"], "register_button", array());
                echo "
                                ";
                // line 36
                echo $this->getAttribute($context["item"], "unsubscribe_button", array());
                echo "
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "default/layout/hot_course_item.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 36,  103 => 35,  98 => 32,  92 => 30,  90 => 29,  83 => 25,  75 => 20,  65 => 19,  56 => 14,  49 => 11,  47 => 10,  39 => 7,  31 => 6,  26 => 3,  23 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/layout/hot_course_item.tpl", "/var/www/mylms/main/template/default/layout/hot_course_item.tpl");
    }
}
