<?php

/* default/course_progress/layout.tpl */
class __TwigTemplate_43bdf0716903142df1c97b28339e1f19c673dcec9054e6233248fbbd9e49a586 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row\">
    <div class=\"col-md-12\">
        ";
        // line 3
        echo ($context["actions"] ?? null);
        echo "
        <div class=\"blog\">
            ";
        // line 5
        echo ($context["content"] ?? null);
        echo "
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "default/course_progress/layout.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 5,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/course_progress/layout.tpl", "/var/www/mylms/main/template/default/course_progress/layout.tpl");
    }
}
