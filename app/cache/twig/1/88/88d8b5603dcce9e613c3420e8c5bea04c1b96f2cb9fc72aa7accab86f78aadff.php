<?php

/* default/social/personal_data.tpl */
class __TwigTemplate_8ba29ace52f576574e0522f180a34788e13f0615b09256cf75bc33b60eefbb7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(Template::findTemplateFilePath("layout/layout_1_col.tpl"), "default/social/personal_data.tpl", 1);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["display"] = $this->loadTemplate(Template::findTemplateFilePath("macro/macro.tpl"), "default/social/personal_data.tpl", 2);
        // line 1
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "<div class=\"row\">
    <div class=\"col-md-3\">
        <div class=\"social-network-menu\">
            ";
        // line 8
        echo ($context["social_avatar_block"] ?? null);
        echo "
            ";
        // line 9
        echo ($context["social_menu_block"] ?? null);
        echo "
        </div>
    </div>
    <div class=\"col-md-9\">
        ";
        // line 13
        echo $context["display"]->getpanel(get_lang("PersonalDataIntroductionTitle"), get_lang("PersonalDataIntroductionText"));
        echo "
        ";
        // line 14
        echo $context["display"]->getpanel(get_lang("PersonalDataKeptOnYou"), $this->getAttribute(($context["personal_data"] ?? null), "data", array()));
        echo "

        ";
        // line 16
        if ($this->getAttribute(($context["personal_data"] ?? null), "responsible", array())) {
            // line 17
            echo "            ";
            echo $context["display"]->getpanel(get_lang("PersonalDataResponsibleOrganizationTitle"), $this->getAttribute(($context["personal_data"] ?? null), "responsible", array()));
            echo "
        ";
        }
        // line 19
        echo "
        ";
        // line 20
        if ($this->getAttribute(($context["personal_data"] ?? null), "treatment", array())) {
            // line 21
            echo "        <div class=\"panel personal-data-treatment\">
            <div class=\"panel-title\">";
            // line 22
            echo get_lang("PersonalDataTreatmentTitle");
            echo "</div>
            <div class=\"personal-data-treatment-description\">
                ";
            // line 24
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["personal_data"] ?? null), "treatment", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["treatment"]) {
                // line 25
                echo "                    ";
                if ($this->getAttribute($context["treatment"], "content", array())) {
                    // line 26
                    echo "                    <div class=\"sub-section\">
                        <div class=\"panel-sub-title\">";
                    // line 27
                    echo $this->getAttribute($context["treatment"], "title", array());
                    echo "</div>
                        <div class=\"panel-body\">";
                    // line 28
                    echo $this->getAttribute($context["treatment"], "content", array());
                    echo "</div>
                    </div>
                    ";
                }
                // line 31
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['treatment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "            </div>
        </div>
        ";
        }
        // line 35
        echo "        ";
        if ($this->getAttribute(($context["personal_data"] ?? null), "officer_name", array())) {
            // line 36
            echo "            <div class=\"panel personal-data-responsible\">
                <div class=\"panel-title\">";
            // line 37
            echo get_lang("PersonalDataOfficerName");
            echo "</div>
                <div class=\"personal-data-responsible-description\">
                    <a href=\"mailto:";
            // line 39
            echo $this->getAttribute(($context["personal_data"] ?? null), "officer_email", array());
            echo "\">";
            echo $this->getAttribute(($context["personal_data"] ?? null), "officer_name", array());
            echo "</a>
                </div>
                <div class=\"panel-title\">";
            // line 41
            echo get_lang("PersonalDataOfficerRole");
            echo "</div>
                <div class=\"personal-data-responsible-description\">
                    ";
            // line 43
            echo $this->getAttribute(($context["personal_data"] ?? null), "officer_role", array());
            echo "
                </div>
            </div>
        ";
        }
        // line 47
        echo "
        ";
        // line 48
        if (($context["term_link"] ?? null)) {
            // line 49
            echo "            ";
            echo $context["display"]->getpanel(get_lang("TermsAndConditions"), ($context["term_link"] ?? null));
            echo "
        ";
        }
        // line 51
        echo "
        ";
        // line 52
        echo $context["display"]->getpanel(get_lang("PersonalDataPermissionsYouGaveUs"), ($context["permission"] ?? null));
        echo "
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "default/social/personal_data.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 52,  151 => 51,  145 => 49,  143 => 48,  140 => 47,  133 => 43,  128 => 41,  121 => 39,  116 => 37,  113 => 36,  110 => 35,  105 => 32,  99 => 31,  93 => 28,  89 => 27,  86 => 26,  83 => 25,  79 => 24,  74 => 22,  71 => 21,  69 => 20,  66 => 19,  60 => 17,  58 => 16,  53 => 14,  49 => 13,  42 => 9,  38 => 8,  33 => 5,  30 => 4,  26 => 1,  24 => 2,  18 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/social/personal_data.tpl", "/var/www/mylms/main/template/default/social/personal_data.tpl");
    }
}
