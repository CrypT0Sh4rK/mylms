<?php

/* default/course_description/index.tpl */
class __TwigTemplate_7998940b88cb334454ef6612b47803970a2ee1ffc66f9eaf845f936ee882eeaa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["messages"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 2
            echo "    ";
            echo $context["message"];
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["listing"] ?? null), "descriptions", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["description"]) {
            // line 5
            echo "    ";
            if ( !twig_test_empty($context["description"])) {
                // line 6
                echo "        <div id=\"description_";
                echo $this->getAttribute($context["description"], "description_type", array());
                echo "\" class=\"panel panel-default\"
             data-id=\"";
                // line 7
                echo $this->getAttribute($context["description"], "id", array());
                echo "\" data-c_id=\"";
                echo $this->getAttribute($context["description"], "c_id", array());
                echo "\" data-type=\"course_description\">
            <div class=\"panel-heading\">
                ";
                // line 9
                if (($context["is_allowed_to_edit"] ?? null)) {
                    // line 10
                    echo "                    <div class=\"pull-right\">
                        ";
                    // line 11
                    if ((($context["session_id"] ?? null) == $this->getAttribute($context["description"], "session_id", array()))) {
                        // line 12
                        echo "                            <a href=\"";
                        echo $this->getAttribute(($context["_p"] ?? null), "web_self", array());
                        echo "?action=edit&amp;id=";
                        echo $this->getAttribute($context["description"], "id", array());
                        echo "&amp;";
                        echo $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array());
                        echo "\"
                               title=\"";
                        // line 13
                        echo get_lang("Edit");
                        echo "\">
                                <img src=\"";
                        // line 14
                        echo Template::get_icon_path("edit.png", 22);
                        echo "\"/>
                            </a>
                            <a href=\"";
                        // line 16
                        echo $this->getAttribute(($context["_p"] ?? null), "web_self", array());
                        echo "?action=delete&amp;id=";
                        echo $this->getAttribute($context["description"], "id", array());
                        echo "&amp;";
                        echo $this->getAttribute(($context["_p"] ?? null), "web_cid_query", array());
                        echo "\"
                               onclick=\"javascript:return confirmation('";
                        // line 17
                        echo $this->getAttribute($context["description"], "title_js", array());
                        echo "');\"
                               title=\"";
                        // line 18
                        echo get_lang("Delete");
                        echo "\">
                                <img src=\"";
                        // line 19
                        echo Template::get_icon_path("delete.png", 22);
                        echo "\"/>
                            </a>
                        ";
                    } else {
                        // line 22
                        echo "                            <img title=\"";
                        echo get_lang("EditionNotAvailableFromSession");
                        echo "\"
                                 alt=\"";
                        // line 23
                        echo get_lang("EditionNotAvailableFromSession");
                        echo "\"
                                 src=\"";
                        // line 24
                        echo Template::get_icon_path("edit_na.png", 22);
                        echo "\" width=\"22\" height=\"22\"
                                 style=\"vertical-align:middle;\">
                        ";
                    }
                    // line 27
                    echo "                    </div>
                ";
                }
                // line 29
                echo "                ";
                echo $this->getAttribute($context["description"], "title", array());
                echo "
            </div>
            <div class=\"panel-body\">
                ";
                // line 32
                echo $this->getAttribute($context["description"], "content", array());
                echo "
            </div>
        </div>
    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['description'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "default/course_description/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 32,  117 => 29,  113 => 27,  107 => 24,  103 => 23,  98 => 22,  92 => 19,  88 => 18,  84 => 17,  76 => 16,  71 => 14,  67 => 13,  58 => 12,  56 => 11,  53 => 10,  51 => 9,  44 => 7,  39 => 6,  36 => 5,  32 => 4,  23 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/course_description/index.tpl", "/var/www/mylms/main/template/default/course_description/index.tpl");
    }
}
