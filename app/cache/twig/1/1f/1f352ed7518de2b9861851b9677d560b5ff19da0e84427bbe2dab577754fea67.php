<?php

/* default/auth/inscription.tpl */
class __TwigTemplate_13cfb7ca4ca2519730f9bb86b32113ae1077be900255b3b4b06445d8b57f10ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 4
        return $this->loadTemplate((((        // line 2
($context["hide_header"] ?? null) == true)) ? (Template::findTemplateFilePath("layout/blank.tpl")) : (Template::findTemplateFilePath("layout/layout_1_col.tpl"))), "default/auth/inscription.tpl", 4);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
";
        // line 9
        echo ($context["inscription_header"] ?? null);
        echo "
";
        // line 10
        echo ($context["inscription_content"] ?? null);
        echo "
";
        // line 11
        echo ($context["form"] ?? null);
        echo "
";
        // line 12
        echo ($context["text_after_registration"] ?? null);
        echo "

";
    }

    public function getTemplateName()
    {
        return "default/auth/inscription.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 12,  43 => 11,  39 => 10,  35 => 9,  32 => 8,  29 => 7,  25 => 4,  19 => 2,  18 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/auth/inscription.tpl", "/var/www/mylms/main/template/default/auth/inscription.tpl");
    }
}
