<?php

/* default/my_space/index.tpl */
class __TwigTemplate_4f5ed372f8be9610571e3f89db6481797bca3b3cea734e46140c999d07f1b7ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"search-student\">
    ";
        // line 2
        echo ($context["form"] ?? null);
        echo "
</div>

<div class=\"page-header\">
    <h4>
        ";
        // line 7
        echo get_lang("Overview");
        echo "
    </h4>
</div>

<div class=\"view-global-followed\">
    <div class=\"row\">
        <div class=\"col-lg-3 col-sm-3\">
            <div class=\"card\">
                <div class=\"content\">
                    <div class=\"card-title\"><a href=\"";
        // line 16
        echo $this->getAttribute(($context["_p"] ?? null), "web_main", array());
        echo "mySpace/student.php\">";
        echo get_lang("FollowedStudents");
        echo "</a></div>
                    <div class=\"row\">
                        <div class=\"col-xs-5\">
                            <div class=\"icon-big icon-student text-center\">
                                <i class=\"fa fa-graduation-cap\" aria-hidden=\"true\"></i>
                            </div>
                        </div>
                        <div class=\"col-xs-7\">
                            <div class=\"numbers\">

                                <h2>";
        // line 26
        echo ($context["students"] ?? null);
        echo "</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-3 col-sm-3\">
            <div class=\"card\">
                <div class=\"content\">
                    <div class=\"card-title\"><a href=\"";
        // line 36
        echo $this->getAttribute(($context["_p"] ?? null), "web_main", array());
        echo "mySpace/users.php?status=";
        echo ($context["studentboss"] ?? null);
        echo "\">";
        echo get_lang("FollowedStudentBosses");
        echo "</a></div>
                    <div class=\"row\">
                        <div class=\"col-xs-5\">
                            <div class=\"icon-big icon-studentboss text-center\">
                                <i class=\"fa fa-address-book\" aria-hidden=\"true\"></i>
                            </div>
                        </div>
                        <div class=\"col-xs-7\">
                            <div class=\"numbers\">

                                <h2>";
        // line 46
        echo ($context["studentbosses"] ?? null);
        echo "</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-3 col-sm-3\">
            <div class=\"card\">
                <div class=\"content\">
                    <div class=\"card-title\"><a href=\"";
        // line 56
        echo $this->getAttribute(($context["_p"] ?? null), "web_main", array());
        echo "mySpace/teachers.php\">";
        echo get_lang("FollowedTeachers");
        echo "</a></div>
                    <div class=\"row\">
                        <div class=\"col-xs-5\">
                            <div class=\"icon-big icon-teachers text-center\">
                                <i class=\"fa fa-book\" aria-hidden=\"true\"></i>
                            </div>
                        </div>
                        <div class=\"col-xs-7\">
                            <div class=\"numbers\">

                                <h2>";
        // line 66
        echo ($context["numberTeachers"] ?? null);
        echo "</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-lg-3 col-sm-3\">
            <div class=\"card\">
                <div class=\"content\">
                    <div class=\"card-title\"><a href=\"";
        // line 76
        echo $this->getAttribute(($context["_p"] ?? null), "web_main", array());
        echo "mySpace/users.php?status=";
        echo ($context["drh"] ?? null);
        echo "\">";
        echo get_lang("FollowedHumanResources");
        echo "</a></div>
                    <div class=\"row\">
                        <div class=\"col-xs-5\">
                            <div class=\"icon-big icon-humanresources text-center\">
                                <i class=\"fa fa-user\" aria-hidden=\"true\"></i>
                            </div>
                        </div>
                        <div class=\"col-xs-7\">
                            <div class=\"numbers\">
                                <h2>";
        // line 85
        echo ($context["humanresources"] ?? null);
        echo "</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class=\"view-global\">
    <div class=\"panel panel-default panel-view\">
        <div class=\"panel-body\">
            <div class=\"row\">
                <div class=\"col-lg-3 col-sm-3\">
                    <div class=\"easy-donut\">
                        <div id=\"easypiechart-blue\" class=\"easypiechart\" data-percent=\"";
        // line 100
        echo ($context["total_user"] ?? null);
        echo "\">
                            <span class=\"percent\">";
        // line 101
        echo ($context["total_user"] ?? null);
        echo "</span>
                        </div>
                        <div class=\"easypiechart-link\">
                            <a class=\"btn btn-default\" href=\"";
        // line 104
        echo $this->getAttribute(($context["_p"] ?? null), "web_main", array());
        echo "mySpace/users.php\">
                                ";
        // line 105
        echo get_lang("FollowedUsers");
        echo "
                            </a>
                        </div>
                    </div>

                    ";
        // line 110
        if (($this->getAttribute(($context["_u"] ?? null), "status", array()) == 1)) {
            // line 111
            echo "                        <a href=\"";
            echo $this->getAttribute(($context["_p"] ?? null), "web_main", array());
            echo "admin/dashboard_add_users_to_user.php?user=";
            echo $this->getAttribute(($context["_u"] ?? null), "id", array());
            echo "\" class=\"btn btn-default btn-sm\">
                            <i class=\"fa fa-plus\" aria-hidden=\"true\"></i>
                        </a>
                    ";
        }
        // line 115
        echo "                </div>
                <div class=\"col-lg-3 col-sm-3\">
                    <div class=\"easy-donut\">
                        <div id=\"easypiechart-orange\" class=\"easypiechart\" data-percent=\"";
        // line 118
        echo $this->getAttribute(($context["stats"] ?? null), "courses", array());
        echo "\">
                            <span class=\"percent\">";
        // line 119
        echo $this->getAttribute(($context["stats"] ?? null), "courses", array());
        echo "</span>
                        </div>
                        <div class=\"easypiechart-link\">
                            <a class=\"btn btn-default\" href=\"";
        // line 122
        echo $this->getAttribute(($context["_p"] ?? null), "web_main", array());
        echo "mySpace/course.php\">
                                ";
        // line 123
        echo get_lang("AssignedCourses");
        echo "
                            </a>
                        </div>
                    </div>
                    ";
        // line 127
        if (($this->getAttribute(($context["_u"] ?? null), "status", array()) == 1)) {
            // line 128
            echo "                        <a href=\"";
            echo $this->getAttribute(($context["_p"] ?? null), "web_main", array());
            echo "mySpace/course.php\" class=\"btn btn-default btn-sm\">
                            <i class=\"fa fa-plus\" aria-hidden=\"true\"></i>
                        </a>
                    ";
        }
        // line 132
        echo "                </div>
                <div class=\"col-lg-3 col-sm-3\">
                    <div  class=\"easy-donut\">
                        <div id=\"easypiechart-teal\" class=\"easypiechart\" data-percent=\"";
        // line 135
        echo $this->getAttribute(($context["stats"] ?? null), "assigned_courses", array());
        echo "\">
                            <span class=\"percent\">";
        // line 136
        echo $this->getAttribute(($context["stats"] ?? null), "assigned_courses", array());
        echo "</span>
                        </div>
                        <div class=\"easypiechart-link\">
                            <a class=\"btn btn-default\" href=\"";
        // line 139
        echo $this->getAttribute(($context["_p"] ?? null), "web_main", array());
        echo "mySpace/course.php?follow\">";
        echo get_lang("FollowedCourses");
        echo "</a>
                        </div>
                    </div>
                    ";
        // line 142
        if (($this->getAttribute(($context["_u"] ?? null), "status", array()) == 1)) {
            // line 143
            echo "                        <a href=\"";
            echo $this->getAttribute(($context["_p"] ?? null), "web_main", array());
            echo "mySpace/course.php?follow\" class=\"btn btn-default btn-sm\">
                            <i class=\"fa fa-plus\" aria-hidden=\"true\"></i>
                        </a>
                    ";
        }
        // line 147
        echo "                </div>
                <div class=\"col-lg-3 col-sm-3\">
                    <div class=\"easy-donut\">
                        <div id=\"easypiechart-red\" class=\"easypiechart\" data-percent=\"";
        // line 150
        echo twig_length_filter($this->env, $this->getAttribute(($context["stats"] ?? null), "session_list", array()));
        echo "\">
                            <span class=\"percent\">";
        // line 151
        echo twig_length_filter($this->env, $this->getAttribute(($context["stats"] ?? null), "session_list", array()));
        echo "</span>
                        </div>
                        <div class=\"easypiechart-link\">
                            <a class=\"btn btn-default\" href=\"";
        // line 154
        echo $this->getAttribute(($context["_p"] ?? null), "web_main", array());
        echo "mySpace/session.php\">";
        echo get_lang("FollowedSessions");
        echo "</a>
                        </div>
                    </div>
                    ";
        // line 157
        if (($this->getAttribute(($context["_u"] ?? null), "status", array()) == 1)) {
            // line 158
            echo "                        <a href=\"";
            echo $this->getAttribute(($context["_p"] ?? null), "web_main", array());
            echo "admin/dashboard_add_sessions_to_user.php?user=";
            echo $this->getAttribute(($context["_u"] ?? null), "id", array());
            echo "\" class=\"btn btn-default btn-sm\">
                            <i class=\"fa fa-plus\" aria-hidden=\"true\"></i>
                        </a>
                    ";
        }
        // line 162
        echo "                </div>
            </div>
        </div>
    </div>
</div>
<div class=\"page-header\">
    <h4>
        ";
        // line 169
        echo ($context["title"] ?? null);
        echo "
    </h4>
</div>
<div class=\"report-section\">
    <div class=\"row\">
        <div class=\"col-lg-3 col-sm-3\">
            <div class=\"item-report\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-body\">
                        <div class=\"item-report-number\">
                            ";
        // line 179
        echo $this->getAttribute(($context["report"] ?? null), "AverageCoursePerStudent", array());
        echo "
                        </div>
                    </div>
                </div>
                <p>";
        // line 183
        echo get_lang("AverageCoursePerStudent");
        echo "</p>
            </div>
        </div>
        <div class=\"col-lg-3 col-sm-3\">
            <div class=\"item-report\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-body\">
                        <div class=\"item-report-number\">
                            ";
        // line 191
        echo $this->getAttribute(($context["report"] ?? null), "InactivesStudents", array());
        echo "
                        </div>
                    </div>
                </div>
                <p>";
        // line 195
        echo get_lang("InactivesStudents");
        echo "</p>
            </div>
        </div>
        <div class=\"col-lg-3 col-sm-3\">
            <div class=\"item-report\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-body\">
                        <div class=\"item-report-number\">
                            ";
        // line 203
        echo $this->getAttribute(($context["report"] ?? null), "AverageTimeSpentOnThePlatform", array());
        echo "
                        </div>
                    </div>
                </div>
                <p>";
        // line 207
        echo get_lang("AverageTimeSpentOnThePlatform");
        echo "</p>
            </div>
        </div>
        <div class=\"col-lg-3 col-sm-3\">
            <div class=\"item-report\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-body\">
                        <div class=\"item-report-number\">
                            ";
        // line 215
        echo $this->getAttribute(($context["report"] ?? null), "AverageProgressInLearnpath", array());
        echo "
                        </div>
                    </div>
                </div>
                <p>";
        // line 219
        echo get_lang("AverageProgressInLearnpath");
        echo "</p>
            </div>
        </div>
        <div class=\"col-lg-3 col-sm-3\">
            <div class=\"item-report\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-body\">
                        <div class=\"item-report-number\">
                            ";
        // line 227
        echo $this->getAttribute(($context["report"] ?? null), "AvgCourseScore", array());
        echo "
                        </div>

                    </div>
                </div>
                <p>";
        // line 232
        echo get_lang("AvgCourseScore");
        echo "</p>
            </div>
        </div>
        <div class=\"col-lg-3 col-sm-3\">
            <div class=\"item-report\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-body\">
                        <div class=\"item-report-number\">
                            ";
        // line 240
        echo $this->getAttribute(($context["report"] ?? null), "AveragePostsInForum", array());
        echo "
                        </div>

                    </div>
                </div>
                <p> ";
        // line 245
        echo get_lang("AveragePostsInForum");
        echo "</p>
            </div>
        </div>
        <div class=\"col-lg-3 col-sm-3\">
            <div class=\"item-report\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-body\">
                        <div class=\"item-report-number\">
                            ";
        // line 253
        echo $this->getAttribute(($context["report"] ?? null), "AverageAssignments", array());
        echo "
                        </div>

                    </div>
                </div>
                <p> ";
        // line 258
        echo get_lang("AverageAssignments");
        echo "</p>
            </div>
        </div>
    </div>
</div>

<script type=\"text/javascript\">
    \$(function() {
        \$('#easypiechart-teal').easyPieChart({
            scaleColor: false,
            barColor: '#1ebfae',
            lineWidth:8,
            trackColor: '#f2f2f2'
        });
    });

    \$(function() {
        \$('#easypiechart-orange').easyPieChart({
            scaleColor: false,
            barColor: '#ffb53e',
            lineWidth:8,
            trackColor: '#f2f2f2'
        });
    });

    \$(function() {
        \$('#easypiechart-red').easyPieChart({
            scaleColor: false,
            barColor: '#f9243f',
            lineWidth:8,
            trackColor: '#f2f2f2'
        });
    });

    \$(function() {
        \$('#easypiechart-blue').easyPieChart({
            scaleColor: false,
            barColor: '#30a5ff',
            lineWidth:8,
            trackColor: '#f2f2f2'
        });
    });

</script>
";
    }

    public function getTemplateName()
    {
        return "default/my_space/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  440 => 258,  432 => 253,  421 => 245,  413 => 240,  402 => 232,  394 => 227,  383 => 219,  376 => 215,  365 => 207,  358 => 203,  347 => 195,  340 => 191,  329 => 183,  322 => 179,  309 => 169,  300 => 162,  290 => 158,  288 => 157,  280 => 154,  274 => 151,  270 => 150,  265 => 147,  257 => 143,  255 => 142,  247 => 139,  241 => 136,  237 => 135,  232 => 132,  224 => 128,  222 => 127,  215 => 123,  211 => 122,  205 => 119,  201 => 118,  196 => 115,  186 => 111,  184 => 110,  176 => 105,  172 => 104,  166 => 101,  162 => 100,  144 => 85,  128 => 76,  115 => 66,  100 => 56,  87 => 46,  70 => 36,  57 => 26,  42 => 16,  30 => 7,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/my_space/index.tpl", "/var/www/mylms/main/template/default/my_space/index.tpl");
    }
}
