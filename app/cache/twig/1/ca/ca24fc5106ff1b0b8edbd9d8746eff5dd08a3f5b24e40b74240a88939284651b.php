<?php

/* default/learnpath/scorm_list.tpl */
class __TwigTemplate_915151c034de5e25dc03aa3221413e48a466b0ed4fcefe5ac170cd37700a3478 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ( !twig_test_empty(($context["data_list"] ?? null))) {
            // line 2
            echo "<div id=\"learning_path_toc\" class=\"scorm-list\">
    <div class=\"scorm-title\">
        <h4>";
            // line 4
            echo ($context["lp_title_scorm"] ?? null);
            echo "</h4>
    </div>
    <div class=\"scorm-body\">
        <div id=\"inner_lp_toc\" class=\"inner_lp_toc scrollbar-light\">
            ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["data_list"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 9
                echo "            <div id=\"toc_";
                echo $this->getAttribute($context["item"], "id", array());
                echo "\" class=\"";
                echo $this->getAttribute($context["item"], "class", array());
                echo " item-";
                echo $this->getAttribute($context["item"], "type", array());
                echo "\">
                ";
                // line 10
                if (($this->getAttribute($context["item"], "type", array()) == "dir")) {
                    // line 11
                    echo "                <div class=\"section ";
                    echo $this->getAttribute($context["item"], "css_level", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["item"], "description", array());
                    echo "\">
                    ";
                    // line 12
                    echo $this->getAttribute($context["item"], "title", array());
                    echo "
                </div>
                ";
                } else {
                    // line 15
                    echo "                <div class=\"item ";
                    echo $this->getAttribute($context["item"], "css_level", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["item"], "description", array());
                    echo "\">
                    <a name=\"atoc_";
                    // line 16
                    echo $this->getAttribute($context["item"], "id", array());
                    echo "\"></a>
                    <a class=\"items-list\" href=\"#\"
                       onclick=\"switch_item('";
                    // line 18
                    echo $this->getAttribute($context["item"], "current_id", array());
                    echo "','";
                    echo $this->getAttribute($context["item"], "id", array());
                    echo "'); return false;\">
                        ";
                    // line 19
                    echo $this->getAttribute($context["item"], "title", array());
                    echo "
                    </a>
                </div>
                ";
                }
                // line 23
                echo "            </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "        </div>
    </div>
</div>
";
        }
        // line 29
        if ( !twig_test_empty(($context["data_panel"] ?? null))) {
            // line 30
            echo "<div id=\"learning_path_toc\" class=\"scorm-collapse\">
    <div class=\"scorm-title\">
        <h4>
             ";
            // line 33
            echo ($context["lp_title_scorm"] ?? null);
            echo "
        </h4>
    </div>
    <div class=\"panel-group\" role=\"tablist\" aria-multiselectable=\"true\">
        ";
            // line 37
            if ($this->getAttribute(($context["data_panel"] ?? null), "not_parents", array())) {
                // line 38
                echo "            <ul class=\"scorm-collapse-list\">
                ";
                // line 39
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["data_panel"] ?? null), "not_parents", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 40
                    echo "                <li id=\"toc_";
                    echo $this->getAttribute($context["item"], "id", array());
                    echo "\" class=\"";
                    echo $this->getAttribute($context["item"], "class", array());
                    echo " item-";
                    echo $this->getAttribute($context["item"], "type", array());
                    echo "\">
                    <div class=\"sub-item type-";
                    // line 41
                    echo $this->getAttribute($context["item"], "type", array());
                    echo "\">
                        <a name=\"atoc_";
                    // line 42
                    echo $this->getAttribute($context["item"], "id", array());
                    echo "\"></a>
                        <a class=\"item-action\" href=\"#\"
                           onclick=\"switch_item('";
                    // line 44
                    echo $this->getAttribute($context["item"], "current_id", array());
                    echo "','";
                    echo $this->getAttribute($context["item"], "id", array());
                    echo "'); return false;\">
                            <i class=\"fa fa-chevron-circle-right\" aria-hidden=\"true\"></i>
                            ";
                    // line 46
                    echo $this->getAttribute($context["item"], "title", array());
                    echo "
                        </a>
                    </div>
                </li>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 51
                echo "            </ul>
        ";
            }
            // line 53
            echo "
        ";
            // line 54
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["data_panel"] ?? null), "are_parents", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 55
                echo "
        <div class=\"panel panel-default ";
                // line 56
                echo (($this->getAttribute($context["item"], "parent", array())) ? ("lower") : ("higher"));
                echo "\" data-lp-id=\"";
                echo $this->getAttribute($context["item"], "id", array());
                echo "\"
             ";
                // line 57
                echo (($this->getAttribute($context["item"], "parent", array())) ? ((("data-lp-parent=\"" . $this->getAttribute($context["item"], "parent", array())) . "\"")) : (""));
                echo ">
            <div class=\"status-heading\">
                <div class=\"panel-heading\" role=\"tab\" id=\"heading-";
                // line 59
                echo $this->getAttribute($context["item"], "id", array());
                echo "\">
                    <h4>
                        <a class=\"item-header\" role=\"button\" data-toggle=\"collapse\"
                            data-parent=\"#scorm-panel";
                // line 62
                echo (($this->getAttribute($context["item"], "parent", array())) ? (("-" . $this->getAttribute($context["item"], "parent", array()))) : (""));
                echo "\"
                            href=\"#collapse-";
                // line 63
                echo $this->getAttribute($context["item"], "id", array());
                echo "\" aria-expanded=\"true\"
                            aria-controls=\"collapse-";
                // line 64
                echo $this->getAttribute($context["item"], "id", array());
                echo "\">
                            ";
                // line 65
                echo $this->getAttribute($context["item"], "title", array());
                echo "
                        </a>
                    </h4>
                </div>
            </div>
            <div id=\"collapse-";
                // line 70
                echo $this->getAttribute($context["item"], "id", array());
                echo "\" class=\"panel-collapse collapse ";
                echo $this->getAttribute($context["item"], "parent_current", array());
                echo "\"
                 role=\"tabpanel\" aria-labelledby=\"heading-";
                // line 71
                echo $this->getAttribute($context["item"], "id", array());
                echo "\">
                <div class=\"panel-body\">
                    <ul class=\"list\">
                        ";
                // line 74
                $context["counter"] = 0;
                // line 75
                echo "                        ";
                $context["final"] = twig_length_filter($this->env, $this->getAttribute($context["item"], "children", array()));
                // line 76
                echo "                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["item"], "children", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["subitem"]) {
                    // line 77
                    echo "                        ";
                    $context["counter"] = (($context["counter"] ?? null) + 1);
                    // line 78
                    echo "                        <li id=\"toc_";
                    echo $this->getAttribute($context["subitem"], "id", array());
                    echo "\"
                            class=\"";
                    // line 79
                    echo $this->getAttribute($context["subitem"], "class", array());
                    echo " ";
                    echo $this->getAttribute($context["subitem"], "type", array());
                    echo " ";
                    echo (((($context["counter"] ?? null) == ($context["final"] ?? null))) ? ("final") : (""));
                    echo "\">
                            <div class=\"sub-item item-";
                    // line 80
                    echo $this->getAttribute($context["subitem"], "type", array());
                    echo "\">
                                <a name=\"atoc_";
                    // line 81
                    echo $this->getAttribute($context["subitem"], "id", array());
                    echo "\"></a>
                                <a class=\"item-action\" href=\"#\"
                                   onclick=\"switch_item('";
                    // line 83
                    echo $this->getAttribute($context["subitem"], "current_id", array());
                    echo "','";
                    echo $this->getAttribute($context["subitem"], "id", array());
                    echo "'); return false;\">
                                    <i class=\"fa fa-chevron-circle-right\" aria-hidden=\"true\"></i>
                                    ";
                    // line 85
                    echo $this->getAttribute($context["subitem"], "title", array());
                    echo "
                                </a>
                            </div>
                        </li>

                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subitem'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 91
                echo "                    </ul>
                </div>
            </div>
        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 96
            echo "
    </div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "default/learnpath/scorm_list.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  284 => 96,  274 => 91,  262 => 85,  255 => 83,  250 => 81,  246 => 80,  238 => 79,  233 => 78,  230 => 77,  225 => 76,  222 => 75,  220 => 74,  214 => 71,  208 => 70,  200 => 65,  196 => 64,  192 => 63,  188 => 62,  182 => 59,  177 => 57,  171 => 56,  168 => 55,  164 => 54,  161 => 53,  157 => 51,  146 => 46,  139 => 44,  134 => 42,  130 => 41,  121 => 40,  117 => 39,  114 => 38,  112 => 37,  105 => 33,  100 => 30,  98 => 29,  92 => 25,  85 => 23,  78 => 19,  72 => 18,  67 => 16,  60 => 15,  54 => 12,  47 => 11,  45 => 10,  36 => 9,  32 => 8,  25 => 4,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/learnpath/scorm_list.tpl", "/var/www/mylms/main/template/default/learnpath/scorm_list.tpl");
    }
}
