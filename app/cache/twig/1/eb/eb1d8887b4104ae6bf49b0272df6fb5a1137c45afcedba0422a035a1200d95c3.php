<?php

/* default/agenda/event_list.tpl */
class __TwigTemplate_4115b0f83151145bb7856e30703dfe548b5c668405e7e46924ec53b38b629f4a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo ($context["agenda_actions"] ?? null);
        echo "

<div class=\"panel-group\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["agenda_events"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["event"]) {
            // line 5
            echo "        <div class=\"panel panel-default\">
            <div class=\"panel-heading\" role=\"tab\" id=\"heading-";
            // line 6
            echo $this->getAttribute($context["event"], "id", array());
            echo "\">
                ";
            // line 7
            if ((($context["is_allowed_to_edit"] ?? null) && ($context["show_action"] ?? null))) {
                // line 8
                echo "                    <div class=\"pull-right\">
                        ";
                // line 9
                if (($this->getAttribute($context["event"], "visibility", array()) == 1)) {
                    // line 10
                    echo "                            <a class=\"btn btn-default btn-xs\"
                               href=\"";
                    // line 11
                    if (($context["url"] ?? null)) {
                        echo ($context["url"] ?? null);
                    } else {
                        echo $this->getAttribute($context["event"], "url", array());
                    }
                    echo "&action=change_visibility&visibility=0&id=";
                    echo $this->getAttribute($context["event"], "real_id", array());
                    echo "&type=";
                    echo $this->getAttribute($context["event"], "type", array());
                    echo "\">
                                <img title=\"";
                    // line 12
                    echo "Invisible";
                    echo "\" src=\"";
                    echo Template::get_icon_path("visible.png", 22);
                    echo "\">
                            </a>
                        ";
                } else {
                    // line 15
                    echo "                            ";
                    if ((($this->getAttribute($context["event"], "type", array()) == "course") || ($this->getAttribute($context["event"], "type", array()) == "session"))) {
                        // line 16
                        echo "                                <a class=\"btn btn-default btn-xs\"
                                   href=\"";
                        // line 17
                        if (($context["url"] ?? null)) {
                            echo ($context["url"] ?? null);
                        } else {
                            echo $this->getAttribute($context["event"], "url", array());
                        }
                        echo "&action=change_visibility&visibility=1&id=";
                        echo $this->getAttribute($context["event"], "real_id", array());
                        echo "&type=";
                        echo $this->getAttribute($context["event"], "type", array());
                        echo "\">
                                    <img title=\"";
                        // line 18
                        echo "Visible";
                        echo "\" src=\"";
                        echo Template::get_icon_path("invisible.png", 22);
                        echo "\">
                                </a>
                            ";
                    }
                    // line 21
                    echo "                        ";
                }
                // line 22
                echo "                    </div>
                ";
            }
            // line 24
            echo "
                <h4 class=\"panel-title\">
                    <a class=\"collapsed\" role=\"button\" data-toggle=\"collapse\" data-parent=\"#accordion\"
                       href=\"#collapse-";
            // line 27
            echo $this->getAttribute($context["event"], "id", array());
            echo "\" aria-expanded=\"false\" aria-controls=\"collapse-";
            echo $this->getAttribute($context["event"], "id", array());
            echo "\">
                        ";
            // line 28
            echo $this->getAttribute($context["event"], "title", array());
            echo "
                        <br>
                        <small>
                            ";
            // line 31
            echo $this->getAttribute($context["event"], "start_date_localtime", array());
            echo "

                            &dash;

                            ";
            // line 35
            if ($this->getAttribute($context["event"], "allDay", array())) {
                // line 36
                echo "                                ";
                echo get_lang("AllDay");
                echo "
                            ";
            } else {
                // line 38
                echo "                                ";
                echo $this->getAttribute($context["event"], "end_date_localtime", array());
                echo "
                            ";
            }
            // line 40
            echo "                        </small>
                    </a>
                </h4>
            </div>
            <div id=\"collapse-";
            // line 44
            echo $this->getAttribute($context["event"], "id", array());
            echo "\" class=\"panel-collapse collapse\" role=\"tabpanel\"
                 aria-labelledby=\"heading-";
            // line 45
            echo $this->getAttribute($context["event"], "id", array());
            echo "\">
                <ul class=\"list-group\">
                    ";
            // line 47
            if ($this->getAttribute($context["event"], "description", array())) {
                // line 48
                echo "                        <li class=\"list-group-item\">
                            ";
                // line 49
                echo $this->getAttribute($context["event"], "description", array());
                echo "
                        </li>
                    ";
            }
            // line 52
            echo "
                    ";
            // line 53
            if ($this->getAttribute($context["event"], "comment", array())) {
                // line 54
                echo "                        <li class=\"list-group-item\">
                            ";
                // line 55
                echo $this->getAttribute($context["event"], "comment", array());
                echo "
                        </li>
                    ";
            }
            // line 58
            echo "
                    ";
            // line 59
            if ($this->getAttribute($context["event"], "attachment", array())) {
                // line 60
                echo "                        <li class=\"list-group-item\">";
                echo $this->getAttribute($context["event"], "attachment", array());
                echo "</li>
                    ";
            }
            // line 62
            echo "                </ul>
            </div>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['event'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "default/agenda/event_list.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 66,  188 => 62,  182 => 60,  180 => 59,  177 => 58,  171 => 55,  168 => 54,  166 => 53,  163 => 52,  157 => 49,  154 => 48,  152 => 47,  147 => 45,  143 => 44,  137 => 40,  131 => 38,  125 => 36,  123 => 35,  116 => 31,  110 => 28,  104 => 27,  99 => 24,  95 => 22,  92 => 21,  84 => 18,  72 => 17,  69 => 16,  66 => 15,  58 => 12,  46 => 11,  43 => 10,  41 => 9,  38 => 8,  36 => 7,  32 => 6,  29 => 5,  25 => 4,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/agenda/event_list.tpl", "/var/www/mylms/main/template/default/agenda/event_list.tpl");
    }
}
