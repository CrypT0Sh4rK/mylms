<?php

/* default/course_home/activity.tpl */
class __TwigTemplate_63861052bca1a3e474fe7f2ad91fa062e7cdbdf4376a0f568c7fe11c2372ae47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["blocks"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["block"]) {
            // line 2
            echo "    ";
            if ($this->getAttribute($context["block"], "title", array())) {
                // line 3
                echo "        <div class=\"page-header\">
            <h4 class=\"title-tools\">";
                // line 4
                echo $this->getAttribute($context["block"], "title", array());
                echo "</h4>
        </div>
    ";
            }
            // line 7
            echo "
    <div class=\"row\">
        <div class=\"";
            // line 9
            echo $this->getAttribute($context["block"], "class", array());
            echo "\">
            ";
            // line 10
            if ((api_get_setting("homepage_view") == "activity")) {
                // line 11
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["block"], "content", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 12
                    echo "            <div class=\"offset2 col-md-4 course-tool\">
                ";
                    // line 13
                    echo $this->getAttribute($context["item"], "extra", array());
                    echo "
                ";
                    // line 14
                    echo $this->getAttribute($context["item"], "visibility", array());
                    echo "
                ";
                    // line 15
                    echo $this->getAttribute($context["item"], "icon", array());
                    echo "
                ";
                    // line 16
                    echo $this->getAttribute($context["item"], "link", array());
                    echo "
            </div>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 19
                echo "            ";
            }
            // line 20
            echo "
            ";
            // line 21
            if ((api_get_setting("homepage_view") == "activity_big")) {
                // line 22
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["block"], "content", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 23
                    echo "            <div class=\"col-xs-6 col-sm-4 col-md-3\">
                <div class=\"course-tool\">
                    <div class=\"big_icon\">
                        ";
                    // line 26
                    echo $this->getAttribute($this->getAttribute($context["item"], "tool", array()), "image", array());
                    echo "
                    </div>
                    <div class=\"content\">
                        ";
                    // line 29
                    echo $this->getAttribute($context["item"], "visibility", array());
                    echo "
                        ";
                    // line 30
                    echo $this->getAttribute($context["item"], "extra", array());
                    echo "
                        ";
                    // line 31
                    echo $this->getAttribute($context["item"], "link", array());
                    echo "
                    </div>
                </div>
            </div>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 36
                echo "            ";
            }
            // line 37
            echo "        </div>
    </div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['block'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "default/course_home/activity.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 37,  117 => 36,  106 => 31,  102 => 30,  98 => 29,  92 => 26,  87 => 23,  82 => 22,  80 => 21,  77 => 20,  74 => 19,  65 => 16,  61 => 15,  57 => 14,  53 => 13,  50 => 12,  45 => 11,  43 => 10,  39 => 9,  35 => 7,  29 => 4,  26 => 3,  23 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/course_home/activity.tpl", "/var/www/mylms/main/template/default/course_home/activity.tpl");
    }
}
