<?php

/* default/course_home/about.tpl */
class __TwigTemplate_34fe0c4654deab6871e0519bca333a8c2f6ca4288bfcf91e8ca6582fedd74917 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"about-course\">
    <div id=\"course-info-top\">
        <h2 class=\"session-title\">";
        // line 3
        echo $this->getAttribute(($context["course"] ?? null), "title", array());
        echo "</h2>
        <div class=\"course-short\">
            <ul>
                <li class=\"author\">";
        // line 6
        echo get_lang("Professors");
        echo "</li>
                ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["course"] ?? null), "teachers", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["teacher"]) {
            // line 8
            echo "                    <li>";
            echo $this->getAttribute($context["teacher"], "complete_name", array());
            echo " | </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['teacher'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "            </ul>

        </div>
    </div>

    ";
        // line 15
        $context["course_video"] = "";
        // line 16
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["course"] ?? null), "extra_fields", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["extra_field"]) {
            // line 17
            echo "    ";
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["extra_field"], "value", array()), "getField", array(), "method"), "getVariable", array(), "method") == "video_url")) {
                // line 18
                echo "    ";
                $context["course_video"] = $this->getAttribute($this->getAttribute($context["extra_field"], "value", array()), "getValue", array(), "method");
                // line 19
                echo "    ";
            }
            // line 20
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['extra_field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "
    <div class=\"panel panel-default\">
        <div class=\"panel-body\">
            <div class=\"row\">
                <div class=\"col-sm-5\">
                    ";
        // line 26
        if (($context["course_video"] ?? null)) {
            // line 27
            echo "                    <div class=\"course-video\">
                        <div class=\"embed-responsive embed-responsive-16by9\">
                            ";
            // line 29
            echo $this->getAttribute(($context["essence"] ?? null), "replace", array(0 => ($context["course_video"] ?? null)), "method");
            echo "
                        </div>
                    </div>
                    ";
        } else {
            // line 33
            echo "                    <div class=\"course-image\">
                        <img src=\"";
            // line 34
            echo $this->getAttribute(($context["course"] ?? null), "image", array());
            echo "\" class=\"img-responsive\" />
                    </div>
                    ";
        }
        // line 37
        echo "
                    <div class=\"share-social-media\">
                        <ul class=\"sharing-buttons\">
                            <li>
                                ";
        // line 41
        echo get_lang("ShareWithYourFriends");
        echo "
                            </li>
                            <li>
                                <a href=\"https://www.facebook.com/sharer/sharer.php?u=";
        // line 44
        echo ($context["url"] ?? null);
        echo "\"
                                   target=\"_blank\" class=\"btn btn-facebook btn-inverse btn-xs\">
                                    <em class=\"fa fa-facebook\"></em> Facebook
                                </a>
                            </li>
                            <li>
                                <a href=\"https://twitter.com/home?";
        // line 50
        echo twig_urlencode_filter(array("status" => (($this->getAttribute(($context["course"] ?? null), "title", array()) . " ") . ($context["url"] ?? null))));
        echo "\"
                                   target=\"_blank\" class=\"btn btn-twitter btn-inverse btn-xs\">
                                    <em class=\"fa fa-twitter\"></em> Twitter
                                </a>
                            </li>
                            <li>
                                <a href=\"https://www.linkedin.com/shareArticle?";
        // line 56
        echo twig_urlencode_filter(array("mini" => "true", "url" => ($context["url"] ?? null), "title" => $this->getAttribute(($context["course"] ?? null), "title", array())));
        echo "\"
                                   target=\"_blank\" class=\"btn btn-linkedin btn-inverse btn-xs\">
                                    <em class=\"fa fa-linkedin\"></em> Linkedin
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class=\"col-sm-7\">
                    <div class=\"course-description\">
                        ";
        // line 66
        echo $this->getAttribute(($context["course"] ?? null), "description", array());
        echo "
                    </div>
                </div>
            </div>
            ";
        // line 70
        if ($this->getAttribute(($context["course"] ?? null), "tags", array())) {
            // line 71
            echo "                <div class=\"panel-tags\">

                    <ul class=\"list-inline course-tags\">
                        <li>";
            // line 74
            echo get_lang("Tags");
            echo " :</li>
                        ";
            // line 75
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["course"] ?? null), "tags", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 76
                echo "                            <li class=\"tag-value\">
                                <span>";
                // line 77
                echo $this->getAttribute($context["tag"], "getTag", array());
                echo "</span>
                            </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 80
            echo "                    </ul>
                </div>
            ";
        }
        // line 83
        echo "        </div>
    </div>
    <section id=\"course-info-bottom\" class=\"course\">
        <div class=\"row\">
            <div class=\"col-sm-8\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-body\">
                        <h3 class=\"sub-title\">";
        // line 90
        echo get_lang("CourseInformation");
        echo "</h3>
                        <div class=\"course-information\">
                            ";
        // line 92
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["course"] ?? null), "syllabus", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["topic"]) {
            // line 93
            echo "                                ";
            if (($this->getAttribute($context["topic"], "content", array()) != "")) {
                // line 94
                echo "                                    <div class=\"topics\">
                                        <h4 class=\"title-info\">
                                            <em class=\"fa fa-book\"></em> ";
                // line 96
                echo $this->getAttribute($context["topic"], "title", array());
                echo "
                                        </h4>
                                        <div class=\"content-info\">
                                            ";
                // line 99
                echo $this->getAttribute($context["topic"], "content", array());
                echo "
                                        </div>
                                    </div>
                                ";
            }
            // line 103
            echo "                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['topic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 104
        echo "                        </div>
                    </div>
                </div>
            </div>

            <div class=\"col-sm-4\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-body\">
                        ";
        // line 112
        if ((($context["is_premium"] ?? null) == false)) {
            // line 113
            echo "                            <h5>";
            echo get_lang("CourseSubscription");
            echo "</h5>
                            <div class=\"session-subscribe\">
                                ";
            // line 115
            if (($this->getAttribute(($context["_u"] ?? null), "logged", array()) == 0)) {
                // line 116
                echo "                                    ";
                if ((api_get_setting("allow_registration") != "false")) {
                    // line 117
                    echo "                                        <a href=\"";
                    echo (($this->getAttribute(($context["_p"] ?? null), "web_main", array()) . "auth/inscription.php") . ($context["redirect_to_session"] ?? null));
                    echo "\" class=\"btn btn-success btn-block btn-lg\">
                                            <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i> ";
                    // line 118
                    echo get_lang("SignUp");
                    echo "
                                        </a>
                                    ";
                }
                // line 121
                echo "                                ";
            } elseif ($this->getAttribute(($context["course"] ?? null), "subscription", array())) {
                // line 122
                echo "                                    <a href=\"";
                echo $this->getAttribute(($context["_p"] ?? null), "web", array());
                echo "courses/";
                echo $this->getAttribute(($context["course"] ?? null), "code", array());
                echo "/index.php?id_session=0\" class=\"btn btn-lg btn-success btn-block\">";
                echo get_lang("CourseHomepage");
                echo "</a>
                                ";
            } else {
                // line 124
                echo "                                    <a href=\"";
                echo $this->getAttribute(($context["_p"] ?? null), "web", array());
                echo "courses/";
                echo $this->getAttribute(($context["course"] ?? null), "code", array());
                echo "/index.php?action=subscribe&sec_token=";
                echo ($context["token"] ?? null);
                echo "\" class=\"btn btn-lg btn-success btn-block\">";
                echo get_lang("Subscribe");
                echo "</a>
                                ";
            }
            // line 126
            echo "                            </div>
                        ";
        } else {
            // line 128
            echo "                            <div class=\"session-price\">
                                <div class=\"sale-price\">
                                    ";
            // line 130
            echo get_lang("SalePrice");
            echo "
                                </div>
                                <div class=\"price-text\">
                                    ";
            // line 133
            echo $this->getAttribute(($context["is_premium"] ?? null), "iso_code", array());
            echo " ";
            echo $this->getAttribute(($context["is_premium"] ?? null), "price", array());
            echo "
                                </div>
                                <div class=\"buy-box\">
                                    <a href=\"";
            // line 136
            echo $this->getAttribute(($context["_p"] ?? null), "web", array());
            echo "plugin/buycourses/src/process.php?i=";
            echo $this->getAttribute(($context["is_premium"] ?? null), "product_id", array());
            echo "&t=";
            echo $this->getAttribute(($context["is_premium"] ?? null), "product_type", array());
            echo "\" class=\"btn btn-lg btn-primary btn-block\">";
            echo get_lang("BuyNow");
            echo "</a>
                                </div>
                            </div>
                        ";
        }
        // line 140
        echo "                    </div>
                </div>
                <div class=\"panel panel-default\">
                    <div class=\"panel-body\">
                        <div class=\"panel-teachers\">
                            <h3 class=\"sub-title\">";
        // line 145
        echo get_lang("Coaches");
        echo "</h3>
                        </div>
                        ";
        // line 147
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["course"] ?? null), "teachers", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["teacher"]) {
            // line 148
            echo "                        <div class=\"coach-information\">
                            <div class=\"coach-header\">
                                <div class=\"coach-avatar\">
                                    <img class=\"img-circle img-responsive\" src=\"";
            // line 151
            echo $this->getAttribute($context["teacher"], "image", array());
            echo "\" alt=\"";
            echo $this->getAttribute($context["teacher"], "complete_name", array());
            echo "\">
                                </div>
                                <div class=\"coach-title\">
                                    <h4>";
            // line 154
            echo $this->getAttribute($context["teacher"], "complete_name", array());
            echo "</h4>
                                    <p> ";
            // line 155
            echo $this->getAttribute($context["teacher"], "diploma", array());
            echo "</p>
                                </div>
                            </div>
                            <div class=\"open-area  ";
            // line 158
            echo (((twig_length_filter($this->env, $this->getAttribute(($context["course"] ?? null), "teachers", array())) >= 2)) ? ("open-more") : (" "));
            echo "\">
                                ";
            // line 159
            echo $this->getAttribute($context["teacher"], "openarea", array());
            echo "
                            </div>
                        </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['teacher'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 163
        echo "
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type=\"text/javascript\">
    \$(document).ready(function() {
        \$('.course-information').readmore({
            speed: 100,
            lessLink: '<a class=\"hide-content\" href=\"#\">";
        // line 175
        echo get_lang("SetInvisible");
        echo "</a>',
            moreLink: '<a class=\"read-more\" href=\"#\">";
        // line 176
        echo get_lang("ReadMore");
        echo "</a>',
            collapsedHeight: 730,
            heightMargin: 100
        });
        \$('.open-more').readmore({
            speed: 100,
            lessLink: '<a class=\"hide-content\" href=\"#\">";
        // line 182
        echo get_lang("SetInvisible");
        echo "</a>',
            moreLink: '<a class=\"read-more\" href=\"#\">";
        // line 183
        echo get_lang("ReadMore");
        echo "</a>',
            collapsedHeight: 90,
            heightMargin: 20
        });
    });
</script>";
    }

    public function getTemplateName()
    {
        return "default/course_home/about.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  408 => 183,  404 => 182,  395 => 176,  391 => 175,  377 => 163,  367 => 159,  363 => 158,  357 => 155,  353 => 154,  345 => 151,  340 => 148,  336 => 147,  331 => 145,  324 => 140,  311 => 136,  303 => 133,  297 => 130,  293 => 128,  289 => 126,  277 => 124,  267 => 122,  264 => 121,  258 => 118,  253 => 117,  250 => 116,  248 => 115,  242 => 113,  240 => 112,  230 => 104,  224 => 103,  217 => 99,  211 => 96,  207 => 94,  204 => 93,  200 => 92,  195 => 90,  186 => 83,  181 => 80,  172 => 77,  169 => 76,  165 => 75,  161 => 74,  156 => 71,  154 => 70,  147 => 66,  134 => 56,  125 => 50,  116 => 44,  110 => 41,  104 => 37,  98 => 34,  95 => 33,  88 => 29,  84 => 27,  82 => 26,  75 => 21,  69 => 20,  66 => 19,  63 => 18,  60 => 17,  55 => 16,  53 => 15,  46 => 10,  37 => 8,  33 => 7,  29 => 6,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/course_home/about.tpl", "/var/www/mylms/main/template/default/course_home/about.tpl");
    }
}
